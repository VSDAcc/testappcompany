//
//  GoogleMapsServices.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/10/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import Moya

typealias PlacesCompletion = ([GooglePlace]) -> Void
protocol GoogleMapsServicesInput {
    func queryPlacesNearCoordinate() -> [GooglePlace]
    func updatePlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], onSuccess: @escaping PlacesCompletion) -> Void
}
class GoogleMapsServices: Handler, GoogleMapsServicesInput {
    
    private let provider = APIProvider<APITimeline>()
    
    public func updatePlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String], onSuccess: @escaping PlacesCompletion) -> Void {
        getPlacesNearCoordinate(coordinate, radius: radius, types: types, onSuccess: { [weak self] (places) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                onSuccess(places)
            }
            strongSelf.savePlacesNearCoordinate(places)
        }) { (error) in
            print(error)
        }
    }
    
    public func queryPlacesNearCoordinate() -> [GooglePlace] {
        return AppStorage.getObject(ofType: [GooglePlace].self, forKey: "googleMaps.places", priority: .permanent) ?? []
    }
    
    private func savePlacesNearCoordinate(_ places: [GooglePlace] ) {
        AppStorage.setObject(places, forKey: "googleMaps.places", priority: .permanent)
    }
    
    private func getPlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String],
                                         onSuccess: @escaping (_ places: [GooglePlace]) -> (),
                                         onFailure: @escaping (_ error: String) -> ()) {
        self.provider.request(.fetchPlacesNearCoordinate(coordinate, radius: radius, types: types)) { (result) in
            self.handle(result: result, onSuccess: { (response) in
                do {
                    let placesModel = try JSONDecoder().decode(GoogleMapsResult.self, from: response.data)
                    onSuccess(placesModel.googlePlaces)
                } catch {
                    print(error)
                }
            }, onError: { (error) in
                onFailure(error.localizedDescription)
            })
        }
    }
}

