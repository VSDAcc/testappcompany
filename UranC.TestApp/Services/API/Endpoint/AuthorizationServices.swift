//
//  AuthorizationServices.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/21/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

class AuthorizationServices: Handler {
    
    private let provider = APIProvider<APIProfile>()
    
    func register(email: String, firstName: String, middleName: String, lastName: String, password: String,
                  onSuccess: @escaping () -> (),
                  onFailure: @escaping (_ error: Error) -> ()) {
        self.provider.request(.register(email: email, firstName: firstName, middleName: middleName, lastName: lastName, password: password)) { (result) in
            self.handle(result: result, onSuccess: { (response) in
                DispatchQueue.main.async {
                    onSuccess()
                }
            }, onError: { (error) in
//                let data = Data(error.description.utf8)
//                if let authError = try? JSONDecoder().decode(AuthorizationError.self, from: data) {
//                    DispatchQueue.main.async {
//                        onFailure(authError)
//                    }
//                }
            })
        }
    }
}
