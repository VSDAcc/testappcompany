//
//  APIMaps.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/10/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import Moya

enum APITimeline {
    case fetchPlacesNearCoordinate(_ coordinate: CLLocationCoordinate2D, radius: Double, types:[String])
}
extension APITimeline: MapsTargetType {
    
    var path: String {
        switch self {
        case let .fetchPlacesNearCoordinate(coordinate, radius, types):
            var urlString = "place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(radius)&rankby=prominence&sensor=true&key=\(Configuration.googleApiKey)"
            let typesString = types.count > 0 ? types.joined(separator: "|") : "food"
            urlString += "&types=\(typesString)"
            urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? urlString
            return urlString
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .fetchPlacesNearCoordinate:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .fetchPlacesNearCoordinate:
            return Task.requestParameters(parameters: {
                let parameters = [String : Any]()
                return parameters
            }(), encoding: JSONEncoding.prettyPrinted)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .fetchPlacesNearCoordinate:
            return nil
        }
    }
}
