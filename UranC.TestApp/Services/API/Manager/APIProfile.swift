//
//  APIProfile.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/21/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation
import Moya

enum APIProfile {
    case register(email: String, firstName: String, middleName: String, lastName: String, password: String)
    case authorize(email: String, password: String)
    case accessToken(_ authorizationCode: String)
}

extension APIProfile: ProfileTargetType {
    
    var path: String {
        switch self {
        case .register: return ""
        case .authorize: return ""
        case .accessToken: return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .register, .authorize, .accessToken:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case let .register(email, firstName, middleName, lastName, password):
            return Task.requestParameters(parameters: {
                var parameters = [String : Any]()
                parameters["first_name"] = firstName
                parameters["middle_name"] = middleName
                parameters["last_name"] = lastName
                parameters["email"] = email
                parameters["password"] = password
                return parameters
            }(), encoding: JSONEncoding.prettyPrinted)
        case let .authorize(email, password):
            return Task.requestParameters(parameters: {
                var parameters = [String : Any]()
                parameters["username"] = email
                parameters["password"] = password
                return parameters
            }(), encoding: JSONEncoding.prettyPrinted)
        case let .accessToken(authorizationCode):
            return Task.requestParameters(parameters: {
                var parameters = [String : Any]()
                parameters["authorization_code"] = authorizationCode
                return parameters
            }(), encoding: JSONEncoding.prettyPrinted)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .register, .authorize, .accessToken:
            return ["accept" : "application/json",
                    "Content-type" : "application/json"]
        }
    }
}
