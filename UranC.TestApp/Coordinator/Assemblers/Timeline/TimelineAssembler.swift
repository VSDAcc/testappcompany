//
//  TimelineAssembler.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/21/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation

protocol TimelineAssemblerDelegate: TimelineListAssembler & TimelineDetailAssembler & TodayTaskAssembler & TodayMapsAssembler { }
class TimelineAssembler: TimelineAssemblerDelegate { }

protocol TimelineListAssembler {
    func resolve(with coordinator: TimelineViewModelCoordinatorDelegate?) -> TimelineViewController
    func resolve(with coordinator: TimelineViewModelCoordinatorDelegate?) -> TimelineViewModelInput
    func resolve() -> FileExhibitsLoaderManagerProtocol
}
extension TimelineListAssembler {
    func resolve(with coordinator: TimelineViewModelCoordinatorDelegate?) -> TimelineViewController {
        return TimelineViewController(viewModel: resolve(with: coordinator))
    }
    
    func resolve(with coordinator: TimelineViewModelCoordinatorDelegate?) -> TimelineViewModelInput {
        return TimelineViewModel(resolve(), coordinator)
    }
    
    func resolve() -> FileExhibitsLoaderManagerProtocol {
        return FileExhibitsLoaderManager()
    }
}

protocol TimelineDetailAssembler {
    func resolve(with coordinator: DetailImageViewModelCoordinatorDelegate?, iphoneModel: IphoneListCellModel) -> TimelineDetailViewController
    func resolve(with coordinator: DetailImageViewModelCoordinatorDelegate?, iphoneModel: IphoneListCellModel) -> DetailImageViewModelInput
}
extension TimelineDetailAssembler {
    func resolve(with coordinator: DetailImageViewModelCoordinatorDelegate?, iphoneModel: IphoneListCellModel) -> TimelineDetailViewController {
        return TimelineDetailViewController(viewModel: resolve(with: coordinator, iphoneModel: iphoneModel))
    }
    
    func resolve(with coordinator: DetailImageViewModelCoordinatorDelegate?, iphoneModel: IphoneListCellModel) -> DetailImageViewModelInput {
        return DetailImageViewModel(iphoneModel: iphoneModel, coordinator: coordinator)
    }
}

protocol TodayTaskAssembler {
    func resolve(with coordinator: TodayTasksViewModelCoordinatorDelegate?) -> TodayViewController
    func resolve(with coordinator: TodayTasksViewModelCoordinatorDelegate?) -> TodayTasksViewModelInput
}
extension TodayTaskAssembler {
    func resolve(with coordinator: TodayTasksViewModelCoordinatorDelegate?) -> TodayViewController {
        return TodayViewController(viewModel: resolve(with: coordinator))
    }
    
    func resolve(with coordinator: TodayTasksViewModelCoordinatorDelegate?) -> TodayTasksViewModelInput {
        return TodayTasksViewModel(coordinator)
    }
}

protocol TodayMapsAssembler {
    func resolve(with coordinator: TodayMapsViewModelCoordinatorDelegate?) -> TodayMapsViewController
    func resolve(with coordinator: TodayMapsViewModelCoordinatorDelegate?) -> TodayMapsViewModelInput
    func resolve() -> GoogleMapsServicesInput
}
extension TodayMapsAssembler {
    func resolve(with coordinator: TodayMapsViewModelCoordinatorDelegate?) -> TodayMapsViewController {
        return TodayMapsViewController(viewModel: resolve(with: coordinator))
    }
    
    func resolve(with coordinator: TodayMapsViewModelCoordinatorDelegate?) -> TodayMapsViewModelInput {
        return TodayMapsViewModel(coordinator, resolve())
    }
    
    func resolve() -> GoogleMapsServicesInput {
        return GoogleMapsServices()
    }
}
