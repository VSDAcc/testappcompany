//
//  TimelineCoordinator.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol TimelineCoordinatorDelegate: class {
    
}
class TimelineCoordinator: Coordinator, RootCoordinator {
    
    weak var rootCoordinator: TimelineCoordinatorDelegate?
    
    let rootNavigationController: UINavigationController
    var childCoordinators: [Coordinator] = [Coordinator]()
    fileprivate var timelineViewController: TimelineViewController!
    fileprivate var transitioningDelegate: TimelineViewControllerTransitioningDelegate?
    fileprivate var timelineAssembler: TimelineAssemblerDelegate
    
    init(navigationController: UINavigationController) {
        rootNavigationController = navigationController
        rootNavigationController.modalPresentationStyle = .fullScreen
        rootNavigationController.modalPresentationCapturesStatusBarAppearance = true
        timelineAssembler = TimelineAssembler()
        transitioningDelegate = TimelineViewControllerTransitioningDelegate()
    }
    
    func start() {
        timelineViewController = timelineAssembler.resolve(with: self)
        rootNavigationController.setViewControllers([timelineViewController], animated: false)
        configureNavigationBar()
    }
    
    func showTodayVC() {
        let todayViewController: TodayViewController = timelineAssembler.resolve(with: self)
        rootNavigationController.show(todayViewController, sender: self)
    }
    
    func showTodayMapsVC() {
        let todayMapsVC: TodayMapsViewController = timelineAssembler.resolve(with: self)
        rootNavigationController.show(todayMapsVC, sender: self)
    }
    
    func showTimelineDetailVC(with iphoneModel: IphoneListCellModel) {
        let detailViewController: TimelineDetailViewController = timelineAssembler.resolve(with: self, iphoneModel: iphoneModel)
        detailViewController.animatableIphoneCells = timelineViewController.animatableCells
        rootNavigationController.delegate = transitioningDelegate
        rootNavigationController.show(detailViewController, sender: self)
    }
    
    func dismissViewController(_ animated: Bool) {
        rootNavigationController.popViewController(animated: animated)
        rootNavigationController.delegate = nil
    }
    
    fileprivate func configureNavigationBar() {
        timelineViewController.navigationItem.title = "Timeline"
        rootNavigationController.navigationItem.largeTitleDisplayMode = .never
    }
}
extension TimelineCoordinator: TimelineViewModelCoordinatorDelegate {
    
    func showTimelineDetailViewController(with iphoneModel: IphoneListCellModel) {
        showTimelineDetailVC(with: iphoneModel)
    }
    
    func showTodayViewController() {
        showTodayVC()
    }
}
extension TimelineCoordinator: DetailImageViewModelCoordinatorDelegate {

    func dismissDetailImageViewController(_ animated: Bool) {
        dismissViewController(animated)
    }
}
extension TimelineCoordinator: TodayTasksViewModelCoordinatorDelegate {
    
    func showTodayMapViewController() {
        showTodayMapsVC()
    }
    
    func dismissTodayViewController(_ animated: Bool) {
        dismissViewController(animated)
    }
}
extension TimelineCoordinator: TodayMapsViewModelCoordinatorDelegate {
    
    func dismissMapsViewController(_ animated: Bool) {
        dismissViewController(animated)
    }
}
