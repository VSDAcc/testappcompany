//
//  AppCoordinator.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class AppCoordinator: RootCoordinator, Coordinator {
    
    fileprivate let navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.configureNavigationBar()
    }
    
    func start() {
        showTimelineViewController()
    }
    
    func showTimelineViewController() {
        let timelineCoordinator = TimelineCoordinator(navigationController: navigationController)
        timelineCoordinator.rootCoordinator = self
        timelineCoordinator.start()
        childCoordinators.append(timelineCoordinator)
    }
    
    private func configureNavigationBar() {
        navigationController.setNavigationBarHidden(false, animated: false)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.backgroundColor = .white
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
    }
}
extension AppCoordinator: TimelineCoordinatorDelegate {

}
