//
//  RegistrationProfile.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/15/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

struct RegistrationProfile {
    
    var email: String
    var lastName: String
    var name: String
    var fatherName: String
}
struct AuthorizationData {
    var data: AuthorizationUserCode
}
extension AuthorizationData: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    enum CodingItemKeys: String, CodingKey {
        case item = "item"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let item = try container.nestedContainer(keyedBy: CodingItemKeys.self, forKey: .data)
        let data: AuthorizationUserCode = try item.decodeIfPresent(AuthorizationUserCode.self, forKey: .item) ?? AuthorizationUserCode(authorizationCode: "", accessToken: "", expireTime: 0.0)
        self.init(data: data)
    }
}
struct AuthorizationUserCode {
    
    var authorizationCode: String
    var accessToken: String
    var expireTime: Float
}
extension AuthorizationUserCode: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case code = "authorization_code"
        case expireTime = "expires_at"
        case accessToke = "access_token"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let code = try container.decodeIfPresent(String.self, forKey: .code) ?? ""
        let token = try container.decodeIfPresent(String.self, forKey: .accessToke) ?? ""
        let expireTime = try container.decodeIfPresent(Float.self, forKey: .expireTime) ?? 0.0
        
        self.init(authorizationCode: code, accessToken: token, expireTime: expireTime)
    }
}
struct AuthorizationError {
    var code: Int?
    var firstName: String?
    var middleName: String?
    var lastName: String?
    var password: String?
    var email: String?
}
extension AuthorizationError: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case error = "error"
    }
    
    enum CodingCodeKeys: String, CodingKey {
        case code = "code"
        case message = "msg"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let error = try container.nestedContainer(keyedBy: CodingCodeKeys.self, forKey: .error)
        let code = try error.decodeIfPresent(Int.self, forKey: .code)
        let dictionary: [String: Any] = try error.decode([String: Any].self, forKey: .message)
        var email: String?, password: String?, lastName: String?, middleName: String?, firstName: String?
        dictionary.forEach { (key, value) in
            guard let array = value as? Array<Any> else { return }
            switch key {
            case "email": email = array[0] as? String
            case "password": password = array[0] as? String
            case "last_name": lastName = array[0] as? String
            case "middle_name": middleName = array[0] as? String
            case "first_name": firstName = array[0] as? String
            default: ()
            }
        }
        self.init(code: code, firstName: firstName, middleName: middleName, lastName: lastName, password: password, email: email)
    }
}
