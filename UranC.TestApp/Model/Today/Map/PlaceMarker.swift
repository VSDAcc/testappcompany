//
//  PlaceMarker.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/10/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit
import GoogleMaps

class PlaceMarker: GMSMarker {
    
    let place: GooglePlace

    init(place: GooglePlace) {
        self.place = place
        super.init()
        position = CLLocationCoordinate2DMake(place.latitude, place.longitude)
        icon = UIImage(named: "icon_me")
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
    }
}
