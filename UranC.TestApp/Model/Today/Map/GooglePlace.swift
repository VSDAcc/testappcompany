//
//  GooglePlace.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/10/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation

struct GoogleMapsResult {
    let googlePlaces: [GooglePlace]
}
extension GoogleMapsResult: Codable {
    
    enum CodingKeys: String, CodingKey {
        case result = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let googlePlaces: [GooglePlace] = try container.decodeIfPresent([GooglePlace].self, forKey: .result) ?? []
        
        self.init(googlePlaces: googlePlaces)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(googlePlaces, forKey: .result)
    }
}
struct GooglePlace {
    let name: String
    let latitude: Double
    let longitude: Double
}
extension GooglePlace: Codable {
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
    
    enum CodingKeysGeometry: String, CodingKey {
        case geometry = "geometry"
    }
    
    enum CodingKeysLocation: String, CodingKey {
        case location = "location"
    }
    
    enum CodingKeysCoordinate: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let name: String = try container.decodeIfPresent(String.self, forKey: .name) ?? "empty"
        let geometryContainer = try decoder.container(keyedBy: CodingKeysGeometry.self)
        let locationContainer = try geometryContainer.nestedContainer(keyedBy: CodingKeysLocation.self, forKey: .geometry)
        let coordinateContainer = try locationContainer.nestedContainer(keyedBy: CodingKeysCoordinate.self, forKey: .location)
        let lat = try coordinateContainer.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        let lng = try coordinateContainer.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        self.init(name: name, latitude: lat, longitude: lng)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(name, forKey: .name)
        
        var geometryContainer = encoder.container(keyedBy: CodingKeysGeometry.self)
        var locationContainer = geometryContainer.nestedContainer(keyedBy: CodingKeysLocation.self, forKey: .geometry)
        var coordinateContainer = locationContainer.nestedContainer(keyedBy: CodingKeysCoordinate.self, forKey: .location)
        try coordinateContainer.encodeIfPresent(longitude, forKey: .longitude)
        try coordinateContainer.encodeIfPresent(latitude, forKey: .latitude)
    }
}
