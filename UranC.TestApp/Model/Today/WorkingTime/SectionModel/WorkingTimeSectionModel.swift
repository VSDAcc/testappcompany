//
//  WorkingTimeSectionModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

protocol WorkingTimeContainerSectionModelHandler: WorkingTimeCollectionViewCellHandler {
    
}
class WorkingTimeContainerSectionModel: BaseSectionModel {
    
    weak var delegate: WorkingTimeContainerSectionModelHandler?
    
    public func update(_ models: [WorkingTime]) {
        var tempRows: [CellIdentifiable] = []
        if !models.isEmpty {
            let workingTimeCellModel = WorkingTimeCellModel(title: "Working Time")
            workingTimeCellModel.delegate = delegate
            workingTimeCellModel.loadData(models)
            tempRows = [workingTimeCellModel]
        }
        rows = tempRows
    }
}

class WorkingTimeSectionModel: BaseSectionModel {
    
    public func create(_ models: [WorkingTime]) {
        var tempRows: [CellIdentifiable] = []
        models.forEach { (model) in
            let cellModel = WorkingTimeCollectionViewCellModel(time: model.time, workingDescription: model.workingDescription, statusTitle: model.statusTitle, image: model.image)
            tempRows.append(cellModel)
        }
        rows = tempRows
    }
}
