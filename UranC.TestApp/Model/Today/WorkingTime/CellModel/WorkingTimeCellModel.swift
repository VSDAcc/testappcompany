//
//  WorkingTimeCellModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class WorkingTimeCollectionViewCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return WorkingTimeCollectionViewCell.reuseIdentifier
    }
    
    let time: String
    let workingDescription: String
    let statusTitle: String
    let image: String
    
    init(time: String, workingDescription: String, statusTitle: String, image: String) {
        self.time = time
        self.workingDescription = workingDescription
        self.statusTitle = statusTitle
        self.image = image
    }
}

protocol WorkingTimeCollectionViewCellHandler: class {
    func didSelectWorkingModel(_ model: WorkingTimeCollectionViewCellModel)
}
class WorkingTimeCellModel: BaseCellModel, ViewModelCellPresentable {
    
    override var cellIdentifier: String {
        return WorkingTimeTableViewCell.reuseIdentifier
    }
    
    weak var delegate: WorkingTimeCollectionViewCellHandler?
    
    fileprivate var itemsOffset: CGFloat = 15.0
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var workingTimeSectionModel = WorkingTimeSectionModel()
    var title: String
    
    init(title: String) {
        self.title = title
    }
    
    func loadData(_ models: [WorkingTime]) {
        sections = [workingTimeSectionModel]
        workingTimeSectionModel.create(models)
    }
    
    func didSelectModel(_ model: CellIdentifiable) {
        if let workingModel = model as? WorkingTimeCollectionViewCellModel {
            delegate?.didSelectWorkingModel(workingModel)
        }
    }
}
extension WorkingTimeCellModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}
extension WorkingTimeCellModel: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = selectedItemAt(section: indexPath.section, row: indexPath.row)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellModel.cellIdentifier, for: indexPath) as! IdentifiableCollectionViewCell
        cell.updateModel(cellModel, viewModel: self)
        return cell
    }
}
extension WorkingTimeCellModel: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let cellModel = selectedItemAt(section: indexPath.section, row: indexPath.row)
        didSelectModel(cellModel)
    }
}
extension WorkingTimeCellModel: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIApplication.shared.statusBarOrientation.isLandscape {
            let width: CGFloat = collectionView.bounds.width - itemsOffset * 4
            let height: CGFloat = collectionView.bounds.height - itemsOffset * 2
            let size = CGSize(width: width, height: height)
            return size
        } else {
            let width: CGFloat = collectionView.bounds.width - itemsOffset * 5
            let height: CGFloat = collectionView.bounds.height - itemsOffset * 2
            let size = CGSize(width: width, height: height)
            return size
        }
    }
}
