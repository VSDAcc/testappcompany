//
//  WorkingTime.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/1/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

struct WorkingTime {
    let time: String
    let workingDescription: String
    let statusTitle: String
    let image: String
    
    static func createWorkingTimeModels() -> [WorkingTime] {
        let firstTaskModel = WorkingTime(time: "Today", workingDescription: "Working place in middle of the city", statusTitle: "Now", image: "Venice")
        let secondTaskModel = WorkingTime(time: "Today", workingDescription: "Working place in middle of the city", statusTitle: "Now", image: "Venice")
        let thirdTaskModel = WorkingTime(time: "Today", workingDescription: "Working place in middle of the city", statusTitle: "Now", image: "Venice")
        return [firstTaskModel, secondTaskModel, thirdTaskModel]
    }
}
