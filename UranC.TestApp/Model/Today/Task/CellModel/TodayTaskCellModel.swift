//
//  TodayTaskCellModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/25/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

struct HeightResultVisitor: ModelVisitor {
    
    let width: CGFloat
    
    init(width: CGFloat) {
        self.width = width
    }
    
    func visit(model: TodayTaskCellModel) -> CGFloat {
        let height = String().estimatedSizeFor(text: model.title, width: width).height
        return height
    }
}
struct ColorResultVisitor: ModelVisitor {
    
    func visit(model: TodayTaskCellModel) {
        if model.titleColor == .black {
            model.titleColor = UIColor.random()
        }
    }
}
class TodayTaskCellModel: BaseCellModel, VisitableModel {
    
    override var cellIdentifier: String {
        return TaskTableViewCell.reuseIdentifier
    }
    
    var title: String
    var image: String
    var uuid: String
    var titleColor: UIColor
    
    init(title: String, image: String, uuid: String, titleColor: UIColor = .black) {
        self.title = title
        self.image = image
        self.uuid = uuid
        self.titleColor = titleColor
    }
    
    func accept<V>(visitor: V) -> V.T where V : ModelVisitor {
        return visitor.visit(model: self)
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? TodayTaskCellModel else {
            return false
        }
        let lhs = self
        return lhs.uuid == rhs.uuid
    }
}
protocol TodayTaskTableViewCellHandler: class {
    func didSelectTaskModel(_ model: TodayTaskCellModel)
    func didRemoveTaskModel(_ model: TodayTaskTableViewCellModel)
}
class TodayTaskTableViewCellModel: BaseCellModel, ViewModelCellPresentable {
    
    override var cellIdentifier: String {
        return TaskContainerTableViewCell.reuseIdentifier
    }
    
    weak var delegate: TodayTaskTableViewCellHandler?
    
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var todayTaskSectionModel = TodayTaskSectionModel()
    
    var title: String
    
    init(title: String) {
        self.title = title
    }
    
    func loadData(_ taskModels: [TaskModel]) {
        sections = [todayTaskSectionModel]
        todayTaskSectionModel.create(taskModels)
    }
    
    func append(_ taskModel: TaskModel) {
        todayTaskSectionModel.append(taskModel)
    }
    
    func didSelectModel(_ model: CellIdentifiable) {
        if let taskModel = model as? TodayTaskCellModel {
            delegate?.didSelectTaskModel(taskModel)
        }
    }
}
extension TodayTaskTableViewCellModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func removeItemAt(section: Int, row: Int) {
        sections[section].rows.remove(at: row)
    }
}
extension TodayTaskTableViewCellModel: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let taskModel = selectedItemAt(section: indexPath.section, row: indexPath.row)
        let taskCell = tableView.dequeueReusableCell(withIdentifier: taskModel.cellIdentifier, for: indexPath) as! IdentifiableTableViewCell
        taskCell.updateModel(taskModel, viewModel: self)
        return taskCell
    }
}
extension TodayTaskTableViewCellModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let taskModel = selectedItemAt(section: indexPath.section, row: indexPath.row)
        didSelectModel(taskModel)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let taskModel = selectedItemAt(section: indexPath.section, row: indexPath.row) as? TodayTaskCellModel {
            taskModel.accept(visitor: ColorResultVisitor())
            let animation = AnimationCellFactory.makeFade(duration: 0.1, delayFactor: 0.1)
            let animator = CellAnimator(animation: animation)
            animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        tableView.beginUpdates()
        removeItemAt(section: indexPath.section, row: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
        delegate?.didRemoveTaskModel(self)
    }
}

