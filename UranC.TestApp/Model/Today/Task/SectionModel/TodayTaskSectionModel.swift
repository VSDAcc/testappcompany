//
//  TodayTaskSectionModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/25/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

protocol TodayTaskTableViewSectionModelHandler: TodayTaskTableViewCellHandler { }
class TodayTaskTableViewSectionModel: BaseSectionModel {
    
    weak var delegate: TodayTaskTableViewSectionModelHandler?
    
    public func update(_ taskModels: [TaskModel]) {
        var tempRows: [CellIdentifiable] = []
        if !taskModels.isEmpty {
            let tableViewCellModel = TodayTaskTableViewCellModel(title: "Working Tasks")
            tableViewCellModel.delegate = delegate
            tableViewCellModel.loadData(taskModels)
            tempRows = [tableViewCellModel]
        }
        rows = tempRows
    }
    
    public func append(_ taskModel: TaskModel) {
        guard let model = rows.first as? TodayTaskTableViewCellModel else { return }
        model.append(taskModel)
    }
}

class TodayTaskSectionModel: BaseSectionModel {
    
    public func create(_ taskModels: [TaskModel]) {
        var tempRows: [CellIdentifiable] = []
        taskModels.forEach { (model) in
            let taskCellModel = TodayTaskCellModel(title: model.title, image: model.image, uuid: model.uuid)
            tempRows.append(taskCellModel)
        }
        rows = tempRows
    }
    
    public func append(_ taskModel: TaskModel) {
        let taskCellModel = TodayTaskCellModel(title: taskModel.title, image: taskModel.image, uuid: taskModel.uuid)
        rows.append(taskCellModel)
    }
    
    public func remove(_ model: TodayTaskCellModel) {
        rows.removeObject(obj: model)
    }
}
