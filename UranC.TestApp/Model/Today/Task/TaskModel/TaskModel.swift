//
//  TaskModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

struct TaskModel: Equatable {
    
    let title: String
    let image: String
    let uuid: String = UUID().uuidString
    
    static func == (lhs: TaskModel, rhs: TaskModel) -> Bool {
        return lhs.uuid == rhs.uuid
    }
    
    static func createTaskModels() -> [TaskModel] {
        let firstTaskModel = TaskModel(title: "What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? Harry Potter and the Sacred Text is a new podcast reading Harry Potter, What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? Harry Potter and the Sacred Text is a new podcast reading Harry Potter, What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? What if we read the books we love as if they were sacred texts? What would we learn?", image: "")
        let secondTaskModel = TaskModel(title: "What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? Harry Potter and the Sacred Text is a new podcast reading Harry Potter", image: "")
        let thirdTaskModel = TaskModel(title: "What if we read the books we love as if they were sacred texts? What would we learn? How might they change us? Harry Potter and the Sacred Text is a new podcast reading Harry Potter", image: "")
        return [firstTaskModel, secondTaskModel, thirdTaskModel]
    }
    
    static func createTaskModel() -> TaskModel {
        return TaskModel(title: "What if we read the books we love as if they were sacred texts? What would we learn?What if we read the books we love as if they were sacred texts? What would we learn?", image: "")
    }
}
