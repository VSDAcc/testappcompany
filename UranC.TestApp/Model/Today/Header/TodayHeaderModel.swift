//
//  TodayHeaderModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

class TodayHeaderModel: NSObject {
    
    var title: String
    var date: String
    var imageName: String
    
    init(title: String, date: String, imageName: String) {
        self.title = title
        self.date = date
        self.imageName = imageName
    }
}
