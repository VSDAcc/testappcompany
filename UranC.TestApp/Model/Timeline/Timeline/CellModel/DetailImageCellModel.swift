//
//  DetailImageCellModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/13/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

class DetailImageCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return DetailImageCollectionViewCell.reuseIdentifier
    }
    
    var image: String
    
    init(image: String) {
        self.image = image
    }
}
