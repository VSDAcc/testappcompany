//
//  IphoneListCellModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol IphoneListCellModelHandler: class {
    func iphoneListSelected(_ iphoneList: IphoneListCellModel)
    func iphoneList(_ iphoneList: IphoneListCellModel, didSelectCell cell: UICollectionViewCell)
}
class IphoneListCellModel: BaseCellModel, ViewModelCellPresentable {
    
    override var cellIdentifier: String {
        return IphoneListCollectionViewCell.reuseIdentifier
    }
    
    public weak var delegate: IphoneListCellModelHandler?
    
    fileprivate weak var collectionView: UICollectionView?
    fileprivate let offset: CGFloat = 10.0
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var iphoneImagesSection = IphoneImagesSectionModel()
    
    var contentOffset: CGPoint
    var images: [String]
    var title: String
    var selectedIndex: IndexPath = IndexPath(item: 0, section: 0)
    
    init(_ iphoneList: IphoneList) {
        self.images = iphoneList.images
        self.title = iphoneList.title
        self.contentOffset = .zero
    }
    
    func loadData(_ iphoneList: IphoneList) {
        self.sections = [iphoneImagesSection]
        self.iphoneImagesSection.insertImageItems(iphoneList)
    }
    
    func didSelectModel(_ model: IphoneListCellModel) {
        self.delegate?.iphoneListSelected(model)
    }
    
    func didSelectCollectionView(_ collectionView: UICollectionView, didSelect cell: UICollectionViewCell) {
        self.collectionView = collectionView
        self.delegate?.iphoneList(self, didSelectCell: cell)
    }
    
    func scrollToIndexPath(_ indexPath: IndexPath) {
        self.selectedIndex = indexPath
        self.collectionView?.scrollToItem(at: self.selectedIndex, at: .centeredHorizontally, animated: false)
        self.collectionView?.setNeedsLayout()
        self.collectionView?.layoutIfNeeded()
        if let selectedCell = self.collectionView?.cellForItem(at: self.selectedIndex) {
            self.delegate?.iphoneList(self, didSelectCell: selectedCell)
        }
    }
}
extension IphoneListCellModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}
extension IphoneListCellModel: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let iphoneImageCellModel = selectedItemAt(section: indexPath.section, row: indexPath.row)
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: iphoneImageCellModel.cellIdentifier, for: indexPath) as! IdentifiableCollectionViewCell
        imageCell.updateModel(iphoneImageCellModel, viewModel: self)
        return imageCell
    }
}
extension IphoneListCellModel: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let selectedCell = collectionView.cellForItem(at: indexPath) ?? UICollectionViewCell()
        selectedIndex = indexPath
        didSelectCollectionView(collectionView, didSelect: selectedCell)
        didSelectModel(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let animation = AnimationCellFactory.makeFade(duration: 0.3, delayFactor: 0.1)
        let animator = CellAnimator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: collectionView)
    }
}
extension IphoneListCellModel: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIApplication.shared.statusBarOrientation.isLandscape {
            let width: CGFloat = collectionView.bounds.width - offset * 2
            let height: CGFloat = collectionView.bounds.height - offset * 4
            let size = CGSize(width: width, height: height)
            return size
        } else {
            let width: CGFloat = collectionView.bounds.width - offset * 2
            let height: CGFloat = collectionView.bounds.height - offset * 2
            let size = CGSize(width: width, height: height)
            return size
        }
    }
}
