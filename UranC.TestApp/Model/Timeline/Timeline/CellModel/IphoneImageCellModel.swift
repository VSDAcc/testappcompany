//
//  IphoneImageCellModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/19/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation
class IphoneImageCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return IphoneImageCollectionViewCell.reuseIdentifier
    }
    
    var image: String
    var title: String
    
    init(image: String, title: String) {
        self.image = image
        self.title = title
    }
}
