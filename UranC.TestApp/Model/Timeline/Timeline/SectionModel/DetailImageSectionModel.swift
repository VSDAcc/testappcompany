//
//  DetailImageSectionModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/13/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

class DetailImageSectionModel: BaseSectionModel {
    
    public func updateImagesItem(_ images: [String]) {
        var tempImages = [CellIdentifiable]()
        images.forEach { (image) in
            let imageCellModel = DetailImageCellModel(image: image)
            tempImages.append(imageCellModel)
        }
        rows = tempImages
    }
}
