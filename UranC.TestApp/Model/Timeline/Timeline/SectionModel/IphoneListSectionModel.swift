//
//  IphoneListSectionModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

protocol IphoneListSectionModelHandler: IphoneListCellModelHandler {
    
}
class IphoneListSectionModel: BaseSectionModel {
    
    public weak var delegate: IphoneListSectionModelHandler?
    
    public func appendListItems(_ iphoneList: [IphoneList]) {
        var tempRows: [CellIdentifiable] = []
        iphoneList.forEach { (iphoneModel) in
            let iphoneListCellModel = IphoneListCellModel(iphoneModel)
            iphoneListCellModel.loadData(iphoneModel)
            iphoneListCellModel.delegate = delegate
            tempRows.append(iphoneListCellModel)
        }
        rows = tempRows
    }
}
class IphoneImagesSectionModel: BaseSectionModel {
    
    public func insertImageItems(_ iphoneList: IphoneList) {
        var tempImages = [CellIdentifiable]()
        iphoneList.images.forEach { (image) in
            let iphoneImageCellModel = IphoneImageCellModel(image: image, title: iphoneList.title)
            tempImages.append(iphoneImageCellModel)
        }
        rows = tempImages
    }
}
