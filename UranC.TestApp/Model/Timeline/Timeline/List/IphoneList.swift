//
//  IphoneList.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

struct List {
    
    public let iphoneList: [IphoneList]
    
    init(iphoneList: [IphoneList]) {
        self.iphoneList = iphoneList
    }
}
extension List: Codable {
    
    enum CodingKeys: String, CodingKey {
        case list = "list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let iphoneList: [IphoneList] = try container.decodeIfPresent([IphoneList].self, forKey: .list) ?? []
        
        self.init(iphoneList: iphoneList)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(iphoneList, forKey: .list)
    }
}
struct IphoneList {
    
    public let images: [String]
    public let title: String
    public let age: IphoneAge?
    
    init(images: [String], title: String, age: IphoneAge?) {
        self.images = images
        self.title = title
        self.age = age
    }
}
extension IphoneList: Codable {
    
    enum CodingKeys: String, CodingKey {
        case images = "images"
        case title = "title"
        case age
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let images: [String] = try container.decodeIfPresent([String].self, forKey: .images) ?? []
        let title: String = try container.decodeIfPresent(String.self, forKey: .title) ?? "empty"
        
        let age: IphoneAge = try container.decodeIfPresent(IphoneAge.self, forKey: .age) ?? IphoneAge()
        
        self.init(images: images, title: title, age: age)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(images, forKey: .images)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(age, forKey: .age)
    }
}
struct IphoneAge {
    
    var iphoneAge: Int?
    
    init() { }
    init (age: Int?) {
        self.iphoneAge = age
    }
}
extension IphoneAge: Codable {
    
    enum CodingKeys: String, CodingKey {
        case realAge = "realage"
        case fakeAge = "fakeage"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let iphoneAge = try container.decodeIfPresent(Int.self, forKey: .realAge)
        
        self.init(age: iphoneAge)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(iphoneAge, forKey: .realAge)
    }
}
