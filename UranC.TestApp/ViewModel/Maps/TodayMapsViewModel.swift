//
//  TodayMapsViewModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/5/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

protocol TodayMapsViewModelCoordinatorDelegate: class {
    func dismissMapsViewController(_ animated: Bool)
}
protocol TodayMapsViewModelOutput: class {
    func viewModelDidLoadData(_ viewModel: TodayMapsViewModelInput)
    func viewModelWillLoadData(_ viewModel: TodayMapsViewModelInput)
    func viewModel(_ viewModel: TodayMapsViewModelInput, didLoadMapMarker marker: PlaceMarker)
}
protocol TodayMapsViewModelInput: class {
    var view: TodayMapsViewModelOutput? { get set }
    var coordinator: TodayMapsViewModelCoordinatorDelegate? { get set }
    var places: [GooglePlace] { get set }
    func loadGoogleMapsData()
    func updateGoogleMapsData(_ coordinate: CLLocationCoordinate2D)
}
class TodayMapsViewModel: TodayMapsViewModelInput {
    
    public weak var coordinator: TodayMapsViewModelCoordinatorDelegate?
    public weak var view: TodayMapsViewModelOutput?
    public var places: [GooglePlace] = []
    fileprivate let googleApiServices: GoogleMapsServicesInput
    private let searchRadius: Double = 1000
    private var searchedTypes = ["bakery", "bar", "cafe", "grocery_or_supermarket", "restaurant"]
    
    //MARK:-Loading
    init(_ coordinator: TodayMapsViewModelCoordinatorDelegate? = nil,
         _ googleApiServices: GoogleMapsServicesInput = GoogleMapsServices()) {
        self.googleApiServices = googleApiServices
        self.coordinator = coordinator
    }
    
    public func loadGoogleMapsData() {
        DispatchQueue.global(qos: .userInteractive).async {
            let places = self.googleApiServices.queryPlacesNearCoordinate()
            self.places = places
            places.forEach({ (place) in
                print("lat \(place.latitude), long \(place.longitude)")
                let marker = PlaceMarker(place: place)
                DispatchQueue.main.async {
                    self.view?.viewModel(self, didLoadMapMarker: marker)
                }
            })
        }
    }
    
    public func updateGoogleMapsData(_ coordinate: CLLocationCoordinate2D) {
        googleApiServices.updatePlacesNearCoordinate(coordinate, radius: searchRadius, types: searchedTypes) { [weak self] (places) in
            guard let strongSelf = self else { return }
            strongSelf.places = places
            places.forEach({ (place) in
                let marker = PlaceMarker(place: place)
                DispatchQueue.main.async {
                    strongSelf.view?.viewModel(strongSelf, didLoadMapMarker: marker)
                }
            })
        }
    }
}
