//
//  TimelineViewModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol TimelineViewModelCoordinatorDelegate: class {
    func showTimelineDetailViewController(with iphoneModel: IphoneListCellModel)
    func showTodayViewController()
}
protocol TimelineViewModelOutput: class {
    func viewModelDidLoadData(_ viewModel: TimelineViewModelInput)
    func viewModelWillLoadData(_ viewModel: TimelineViewModelInput)
    func viewModel(_ viewModel: TimelineViewModelInput, didHandleError error: String)
    func viewModel(_ viewModel: TimelineViewModelInput, didSelectCell cell: UICollectionViewCell)
}
protocol TimelineViewModelInput: ViewModelCellPresentable, IphoneListSectionModelHandler {
    func loadData()
    func updateExhibitList()
    func showTodayViewController()
    var coordinator: TimelineViewModelCoordinatorDelegate? { get set }
    var view: TimelineViewModelOutput? { get set }
}
class TimelineViewModel: TimelineViewModelInput {
    
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var iphoneListSection = IphoneListSectionModel()
    fileprivate var fileExhibitsLoaderManager: FileExhibitsLoaderManagerProtocol
    fileprivate let group = DispatchGroup()
    
    public weak var coordinator: TimelineViewModelCoordinatorDelegate?
    public weak var view: TimelineViewModelOutput?
    
    init(_ fileApiService: FileExhibitsLoaderManagerProtocol = FileExhibitsLoaderManager(),
         _ coordinator: TimelineViewModelCoordinatorDelegate? = nil) {
        self.coordinator = coordinator
        fileExhibitsLoaderManager = fileApiService
        iphoneListSection.delegate = self
    }
    //MARK:-TimelineViewModelInput
    func loadData() {
        group.enter()
        sections = [iphoneListSection]
        iphoneListSection.appendListItems(fileExhibitsLoaderManager.queryExhibitList())
        group.leave()
        group.notify(queue: .main) {
            self.view?.viewModelDidLoadData(self)
        }
    }
    
    func updateExhibitList() {
        DispatchQueue.global(qos: .userInteractive).async {
            self.fileExhibitsLoaderManager.updateExhibitsList(onSuccess: { [weak self] (list) in
                guard let strongSelf = self else { return }
                strongSelf.iphoneListSection.appendListItems(list)
                strongSelf.view?.viewModelDidLoadData(strongSelf)
            })
        }
    }
    
    func showTodayViewController() {
        coordinator?.showTodayViewController()
    }
    //MARK:-IphoneListSectionModelHandler
    func iphoneListSelected(_ iphoneList: IphoneListCellModel) {
        coordinator?.showTimelineDetailViewController(with: iphoneList)
    }
    
    func iphoneList(_ iphoneList: IphoneListCellModel, didSelectCell cell: UICollectionViewCell) {
        view?.viewModel(self, didSelectCell: cell)
    }
}
extension TimelineViewModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}
