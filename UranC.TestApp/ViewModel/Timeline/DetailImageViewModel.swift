//
//  DetailImageViewModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/13/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

protocol DetailImageViewModelCoordinatorDelegate: class {
    func dismissDetailImageViewController(_ animated: Bool)
}
protocol DetailImageViewModelOutput: class {
    func viewModelDidLoadData(_ viewModel: DetailImageViewModelInput)
    func viewModelWillLoadData(_ viewModel: DetailImageViewModelInput)
    func viewModel(_ viewModel: DetailImageViewModelInput, didHandleError error: String)
    func viewModel(_ viewModel: DetailImageViewModelInput, didSelectItemAt indexPath: IndexPath)
}
protocol DetailImageViewModelInput: ViewModelCellPresentable {
    var coordinator: DetailImageViewModelCoordinatorDelegate? { get set }
    var view: DetailImageViewModelOutput? { get set }
    var selectedIndex: IndexPath { get set }
    func loadData()
    func dismissDetailImageViewController(_ animated: Bool)
    func iphoneListNeedsToScroll()
}
class DetailImageViewModel: DetailImageViewModelInput {

    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var detailImageSection = DetailImageSectionModel()
    fileprivate let group = DispatchGroup()
    
    public weak var coordinator: DetailImageViewModelCoordinatorDelegate?
    public weak var view: DetailImageViewModelOutput?
    var selectedIndex: IndexPath
    fileprivate var iphoneModel: IphoneListCellModel
    
    //MARK:-Loading
    init(iphoneModel: IphoneListCellModel, coordinator: DetailImageViewModelCoordinatorDelegate? = nil) {
        self.coordinator = coordinator
        self.iphoneModel = iphoneModel
        self.selectedIndex = iphoneModel.selectedIndex
    }
    //MARK:-TimelineViewModelInput
    func loadData() {
        group.enter()
        sections = [detailImageSection]
        detailImageSection.updateImagesItem(iphoneModel.images)
        group.leave()
        group.notify(queue: .main) {
            self.view?.viewModelDidLoadData(self)
        }
    }
    
    func iphoneListNeedsToScroll() {
        iphoneModel.scrollToIndexPath(selectedIndex)
    }
    
    func dismissDetailImageViewController(_ animated: Bool) {
        coordinator?.dismissDetailImageViewController(animated)
    }
}
extension DetailImageViewModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}
