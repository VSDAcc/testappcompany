//
//  TodayTasksViewModel.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/25/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation
import UIKit

protocol TodayTasksViewModelCoordinatorDelegate: class {
    func dismissTodayViewController(_ animated: Bool)
    func showTodayMapViewController()
}
protocol TodayTasksViewModelOutput: class {
    func viewModelDidLoadData(_ viewModel: TodayTasksViewModelInput)
    func viewModel(_ viewModel: TodayTasksViewModelInput, didLoadHeader header: TodayHeaderModel)
    func viewModelScrollToBottom(_ viewModel: TodayTasksViewModelInput)
    func viewModel(_ viewModel: TodayTasksViewModelInput, needsReload section: IndexSet)
}
protocol TodayTasksViewModelInput: ViewModelCellPresentable, TodayTaskTableViewSectionModelHandler, WorkingTimeContainerSectionModelHandler {
    var view: TodayTasksViewModelOutput? { get set }
    var coordinator: TodayTasksViewModelCoordinatorDelegate? { get set }
    func loadData()
    func loadHeaderData()
    func showTodayMapViewController()
}
class TodayTasksViewModel: TodayTasksViewModelInput {
    
    public weak var coordinator: TodayTasksViewModelCoordinatorDelegate?
    public weak var view: TodayTasksViewModelOutput?
    
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var todayTaskTableViewSection = TodayTaskTableViewSectionModel()
    fileprivate var workingTimeSection = WorkingTimeContainerSectionModel()
    fileprivate var todayHeaderModel: TodayHeaderModel?
    fileprivate let group = DispatchGroup()
    
    //MARK:-Loading
    init(_ coordinator: TodayTasksViewModelCoordinatorDelegate? = nil) {
        self.coordinator = coordinator
        workingTimeSection.delegate = self
        todayTaskTableViewSection.delegate = self
        print("pull request test")
    }
    //MARK:-RegistrationViewModelInput
    func loadData() {
        group.enter()
        sections = [workingTimeSection, todayTaskTableViewSection]
        workingTimeSection.update(WorkingTime.createWorkingTimeModels())
        todayTaskTableViewSection.update(TaskModel.createTaskModels())
        group.leave()
        group.notify(queue: .main) {
            self.view?.viewModelDidLoadData(self)
        }
    }
    
    func loadHeaderData() {
        group.enter()
        let todayDate = Date().formatDateToStringWithLongDateStyle()
        todayHeaderModel = TodayHeaderModel(title: "Today", date: todayDate, imageName: "imageName")
        group.leave()
        group.notify(queue: .main) {
            self.view?.viewModel(self, didLoadHeader: self.todayHeaderModel!)
        }
    }
    
    func showTodayMapViewController() {
        coordinator?.showTodayMapViewController()
    }
    //MARK-TodayTaskTableViewCellHandler
    func didSelectTaskModel(_ model: TodayTaskCellModel) {
        group.enter()
        let task = TaskModel.createTaskModel()
        self.todayTaskTableViewSection.append(task)
        group.leave()
        group.notify(queue: .main) {
            self.view?.viewModelDidLoadData(self)
            DispatchQueue.main.async {
                self.view?.viewModelScrollToBottom(self)
            }
        }
    }
    
    func didRemoveTaskModel(_ model: TodayTaskTableViewCellModel) {
        self.view?.viewModelDidLoadData(self)
    }
    //MARK:-WorkingTimeContainerSectionModelHandler
    func didSelectWorkingModel(_ model: WorkingTimeCollectionViewCellModel) {
        print(model.workingDescription)
    }
}
extension TodayTasksViewModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}

