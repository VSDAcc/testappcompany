//
//  Configuration.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation

struct Configuration {
    
    static var debug: Bool {
        return false
    }
    
    static var production: Bool {
        return true
    }
    
    static var testEndpoint: URL {
        return URL(string: "")!
    }
    
    static var productionEndpoint: URL {
        return URL(string: "")!
    }
    
    static var encriptionKey: String{
        return ""
    }
    
    static var requiredEndpoint: URL {
        return production ? productionEndpoint : testEndpoint
    }
    
    static var googleMapsEndpoint: URL {
        return URL(string: "https://maps.googleapis.com/maps/api/")!
    }
    
    //    static var serverTimeDifference: TimeInterval {
    //        return AppStorage.getObject(ofType: TimeInterval.self, forKey: "timeDifference", priority: .permanent) ?? -1
    //    }
    //
    static var login: String {
        return ""
    }
    
    static var password: String {
        return ""
    }
    
    static let googleApiKey: String = "AIzaSyBBn2z5n6NdvHPC-eJ1pIor70-lKbp3N1A"
}
