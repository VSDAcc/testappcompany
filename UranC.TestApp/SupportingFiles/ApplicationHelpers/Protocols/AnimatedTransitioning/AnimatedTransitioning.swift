//
//  AnimatedTransitioning.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/15/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

import UIKit

/**
 extension on required UIViewControllerAnimatedTransitioning protocol to add properties to support spring animation
 */
protocol AnimatedTransitioning: UIViewControllerAnimatedTransitioning {
    var operation: UINavigationController.Operation { get set }
    var duration : TimeInterval { get }
}

protocol ListToDetailAnimatable {
    var morphViews: [UIView] { get }
    var animatableCells: [UICollectionViewCell] { get }
}
