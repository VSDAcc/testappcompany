//
//  DataTaskExtension.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol DataTaskManagerRequest: class {
    typealias completion = (_ success: Data?, _ error: String?) -> ()
    
    var defaultSession: URLSession {get}
    var dataTask: URLSessionDataTask? {get set}
    func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (completion))
}
extension DataTaskManagerRequest {
    
    func dataTask(request: NSMutableURLRequest, method: String, completion: @escaping (completion)) {
        request.httpMethod = method
        
        dataTask = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if let error = error {
                completion(nil, error.localizedDescription)
            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                completion(data, nil)
            } else if let _ = data, let response = response as? HTTPURLResponse, response.statusCode >= 400 {
                do {
                    
                    completion(nil, "error")
                }
            }
        }
        dataTask?.resume()
    }
}
protocol DataTaskRequestServices: DataTaskManagerRequest  {
    
    func post(request: NSMutableURLRequest, completion: @escaping (completion))
    
    func put(request: NSMutableURLRequest, completion: @escaping (completion))
    
    func get(request: NSMutableURLRequest, completion: @escaping (completion))
    
    func clientURLRequest(path: URL?, params: Dictionary<String, Any>?) -> NSMutableURLRequest
}
extension DataTaskRequestServices {
    
    func post(request: NSMutableURLRequest, completion: @escaping (completion)) {
        dataTask(request: request, method: "POST", completion: completion)
    }
    
    func put(request: NSMutableURLRequest, completion: @escaping (completion)) {
        dataTask(request: request, method: "PUT", completion: completion)
    }
    
    func get(request: NSMutableURLRequest, completion: @escaping (completion)) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    func clientURLRequest(path: URL?, params: Dictionary<String, Any>? = nil) -> NSMutableURLRequest {
        let request = NSMutableURLRequest(url: path!)
        return request
    }
}
