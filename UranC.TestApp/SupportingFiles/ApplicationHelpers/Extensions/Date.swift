//
//  Date.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit
import SwiftDate

extension Date {

    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    func stringFormatter(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "uk")
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func stringFromDate(_ date: Date) -> String {
        let date = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func formatDateToStringWithLongDateStyle() -> String {
        let date = Date()
        let calendar = NSCalendar.current
        let dateFormatter = DateFormatter()
        if calendar.isDateInToday(date) {
            dateFormatter.dateStyle = .long
        }else {
            dateFormatter.dateStyle = .long
        }
        dateFormatter.timeStyle = .none
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter.string(from: date)
    }
}
