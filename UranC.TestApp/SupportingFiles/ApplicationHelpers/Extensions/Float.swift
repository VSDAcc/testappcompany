//
//  Float.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 2/27/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
