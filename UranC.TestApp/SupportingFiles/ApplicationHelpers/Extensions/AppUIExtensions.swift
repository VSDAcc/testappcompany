//
//  AppUIExtensions.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import AVKit
import AVFoundation
import SDWebImage

protocol PresenterAlertHandler {
    func presentAlertWith(title: String, massage: String)
}
extension PresenterAlertHandler where Self: UIViewController {
    func presentAlertWith(title: String, massage: String) {
        let alert = UIAlertController(title: title, message: massage, preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(actionCancel)
        present(alert, animated: true, completion: nil)
    }
}
extension UITextField {
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension UIView {
    
    func addShadow(_ color: UIColor, radius: CGFloat = 0.0, offset: CGSize = .zero) {
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.masksToBounds = false
    }
    
    var loadingIndicator: UIActivityIndicatorView? {
        get {
            return self.viewWithTag(9999) as? UIActivityIndicatorView
        }
    }
    
    func loadingIndicator(_ show: Bool, style: UIActivityIndicatorView.Style = .gray) {
        
        if show {
            
            let viewHeight = self.bounds.size.height
            let viewWidth = self.bounds.size.width
            let indicator = (self.viewWithTag(9999) as? UIActivityIndicatorView) ?? UIActivityIndicatorView(style: style)
            indicator.center = CGPoint(x: viewWidth/2, y: viewHeight/2)
            indicator.tag = 9999
            
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            if let indicator = self.loadingIndicator {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
    
    func removeBorders() {
        if let sublayers = self.layer.sublayers {
            for layer in sublayers {
                if layer.name == "topBorder" || layer.name == "bottomBorder" || layer.name == "leftBorder" || layer.name == "rightBorder" {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    func addBorder(_ edges: [UIRectEdge], color: CGColor, width: CGFloat) {
        
        self.removeBorders()
        
        for edge in edges {
            switch edge {
            case UIRectEdge.top:
                
                if let sublayers = self.layer.sublayers {
                    for layer in sublayers {
                        if layer.name == "topBorder" {
                            layer.removeFromSuperlayer()
                        }
                    }
                }
                
                let border = CALayer()
                border.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: width)
                border.backgroundColor = color
                border.name = "topBorder"
                
                self.layer.addSublayer(border)
                
            case UIRectEdge.bottom:
                
                if let sublayers = self.layer.sublayers {
                    for layer in sublayers {
                        if layer.name == "bottomBorder" {
                            layer.removeFromSuperlayer()
                        }
                    }
                }
                
                let border = CALayer()
                border.frame = CGRect(x: 0, y: self.bounds.maxY - width, width: self.bounds.width, height: width)
                border.backgroundColor = color
                border.name = "bottomBorder"
                
                self.layer.addSublayer(border)
                
            case UIRectEdge.left:
                
                if let sublayers = self.layer.sublayers {
                    for layer in sublayers {
                        if layer.name == "leftBorder" {
                            layer.removeFromSuperlayer()
                        }
                    }
                }
                
                let border = CALayer()
                border.frame = CGRect(x: self.bounds.minX, y: 0.0, width: width, height: self.bounds.height)
                border.backgroundColor = color
                border.name = "leftBorder"
                
                self.layer.addSublayer(border)
                
            case UIRectEdge.right:
                
                if let sublayers = self.layer.sublayers {
                    for layer in sublayers {
                        if layer.name == "rightBorder" {
                            layer.removeFromSuperlayer()
                        }
                    }
                }
                
                let border = CALayer()
                border.frame = CGRect(x: self.bounds.maxX - width, y: 0.0, width: width, height: self.bounds.height)
                border.backgroundColor = color
                border.name = "rightBorder"
                
                self.layer.addSublayer(border)
                
            default:
                break
            }
        }
    }
}
extension UIImageView {
    func downloadImageUsingCache(stringURL: String) {
        DispatchQueue.global(qos: .userInteractive).async {
            let imageManager = SDWebImageManager.shared()
            if let image = imageManager.imageCache?.imageFromCache(forKey: stringURL) {
                DispatchQueue.main.async {
                    self.image = image
                }
                return
            } else {
                if let imageURL = URL(string: stringURL) {
                    _ = imageManager.imageDownloader?.downloadImage(with: imageURL, options: [.continueInBackground,.progressiveDownload], progress: nil, completed: {  (image, data, error, completed) in
                        if completed {
                            DispatchQueue.main.async {
                                self.image = image
                            }
                            imageManager.imageCache?.store(image, forKey: stringURL, completion: nil)
                        }
                    })
                }
            }
        }
    }
}
