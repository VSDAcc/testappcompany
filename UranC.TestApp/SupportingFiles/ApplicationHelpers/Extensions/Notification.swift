//
//  Notification.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/30/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let exhibitListDidUpdate = Notification.Name("UranC.Test.com.exhibitListDidUpdate")
}
