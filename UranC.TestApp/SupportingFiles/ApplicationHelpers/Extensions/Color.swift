//
//  Color.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 2/27/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var separatorColor: UIColor {
        return UIColor(r: 230, g: 230, b: 230, alpha: 1)
    }
    
    class var gainsboroColor: UIColor {
        return UIColor(r: 220, g: 220, b: 220, alpha: 1)
    }
    
    class var silverColor: UIColor {
        return UIColor(r: 190, g: 190, b: 190, alpha: 1)
    }
    
    class var systemBlueColor: UIColor {
        return UIColor(r: 0, g: 122, b: 255, alpha: 1)
    }
    
    class var aliceBlue: UIColor {
        return UIColor(r: 248, g: 248, b: 255, alpha: 1)
    }
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: alpha)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
