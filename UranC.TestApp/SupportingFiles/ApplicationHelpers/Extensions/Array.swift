//
//  Array.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 2/10/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {
    
    private func _quickSort(_ array: [Element], by areInIncreasingOrder: ((Element, Element) -> Bool)) -> [Element] {
        if array.count < 2 { return array }
        var data = array
        
        let pivot = data.remove(at: 0)
        let left = data.filter { areInIncreasingOrder($0, pivot) }
        let right = data.filter { !areInIncreasingOrder($0, pivot) }
        let middle = [pivot]
        
        return _quickSort(left, by: areInIncreasingOrder) + middle + _quickSort(right, by: areInIncreasingOrder)
    }
    
    func quickSort(by areInIncreasingOrder: ((Element, Element) -> Bool) = (<)) -> [Element] {
        return _quickSort(self, by: areInIncreasingOrder)
    }
}
extension Array {
    
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
    
    mutating func removeObject<T>(obj: T) where T : Equatable {
        self = self.filter({$0 as? T != obj})
    }
}
