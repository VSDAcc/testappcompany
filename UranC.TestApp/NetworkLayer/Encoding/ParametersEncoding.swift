//
//  ParametersEncoding.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 29.09.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public typealias Parameters = [String: Any]

public protocol ParametersEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
