//
//  JSONParametersEncoder.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 29.09.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public struct JSONParametersEncoder: ParametersEncoder {
    
    public static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            urlRequest.httpBody = jsonData
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}
