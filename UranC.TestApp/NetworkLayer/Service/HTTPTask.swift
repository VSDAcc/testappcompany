//
//  HTTPTask.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 29.09.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]

public enum HTTPTask {
    case request
    
    case requestParameters(_ bodyParameters: Parameters?,_ urlParameters: Parameters?)
    case requestParametersAndHeaders(_ bodyParameters: Parameters?,_ urlParameters: Parameters?,_ additionHeaders: HTTPHeaders?)
}
