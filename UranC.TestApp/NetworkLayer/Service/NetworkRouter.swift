//
//  NetworkRouter.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 29.09.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?) -> ()

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}
