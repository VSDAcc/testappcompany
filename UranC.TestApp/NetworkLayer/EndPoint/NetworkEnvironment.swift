//
//  NetworkEnvironment.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 9/30/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public enum NetworkEnvironment {
    case qa
    case production
    case staging
}
