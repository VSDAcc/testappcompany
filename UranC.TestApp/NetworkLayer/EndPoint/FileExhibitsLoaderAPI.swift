//
//  FileExhibitsLoaderAPI.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 9/30/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public enum FileExhibitsAPI {
    case exhibitList
    case deleteList(page: Int)
}

extension FileExhibitsAPI: EndPointType {
    
    var environmentBaseURL: String {
        switch FileExhibitsLoaderManager.environment {
        case .production: return "https://goo.gl/"
        default:
            return "https://goo.gl/"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.") }
        return url
    }
    
    var path: String {
        switch self {
        case .exhibitList:
            return "t1qKMS"
        default: return "t1qKMS"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .exhibitList:
            return .request
        case .deleteList (let page):
            return .requestParameters(nil, ["page" : page])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
