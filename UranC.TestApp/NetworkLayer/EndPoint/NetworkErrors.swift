//
//  NetworkErrors.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 29.09.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

public enum NetworkError: String, Error {
    case parametersNil = "Parameters were nil"
    case encodingFailed = "Parameters encoding failed"
    case missingURL = "URL is nil"
}
