//
//  FileExhibitsLoaderManager.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 9/30/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

protocol FileExhibitsLoaderManagerProtocol {
    func queryExhibitList() -> [IphoneList]
    func updateExhibitsList(onSuccess: @escaping (_ iphoneList: [IphoneList]) -> ())
}
class FileExhibitsLoaderManager: FileExhibitsLoaderManagerProtocol {
    
    enum NetworkResponse: String {
        case success
        case noData = "Response returned with no data to decode"
        case failed = "Network request failed"
    }
    
    enum Result<String> {
        case success
        case failure(String)
    }
    
    static let environment: NetworkEnvironment = .production
    private let router = Router<FileExhibitsAPI>()
    
    func updateExhibitsList(onSuccess: @escaping (_ iphoneList: [IphoneList]) -> ()) {
        getExhibitsList(onSuccess: { [weak self] (iphoneList) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                onSuccess(iphoneList)
            }
            strongSelf.saveExhibitListToStorage(iphoneList)
        }) { (error) in
            print(error)
        }
    }
    
    func queryExhibitList() -> [IphoneList] {
        return AppStorage.getObject(ofType: [IphoneList].self, forKey: "list.iphoneList", priority: .permanent) ?? []
    }
    
    private func saveExhibitListToStorage(_ list: [IphoneList]) {
        AppStorage.setObject(list, forKey: "list.iphoneList", priority: .permanent)
    }
    
    private func getExhibitsList(onSuccess: @escaping (_ iphoneList: [IphoneList]) -> (),
                                 onFailure: @escaping (_ error: String) -> ()) {
        router.request(.exhibitList) { (data, response, error) in
            if error != nil {
                onFailure(error?.localizedDescription ?? "Please check internet connection")
            }
            
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                
                switch result {
                case .success:
                    guard let data = data else {
                        onFailure(NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        let list = try JSONDecoder().decode(List.self, from: data)
                        onSuccess(list.iphoneList)
                    } catch {
                        onFailure(NetworkResponse.failed.rawValue)
                    }
                case .failure(let networkError):
                    onFailure(networkError)
                }
            }
        }
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
        case 200...299:
             return .success
        default:
            return .failure(NetworkResponse.failed.rawValue)
        }
    }
}
