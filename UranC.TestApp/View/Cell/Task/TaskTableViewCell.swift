//
//  TaskTableViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/25/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class TaskTableViewCell: IdentifiableTableViewCell {

    lazy var taskDescriptionLabel: UILabel = self.createTitleLabel()
    lazy var titleImageView: UIImageView = self.createImageView()
    
    //MARK:-Loading
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllConstraintsToViews()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleImageView.layer.cornerRadius = titleImageView.bounds.width / 2
        taskDescriptionLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
    }
    
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let _ = viewModel as? TodayTaskTableViewCellModel else { return }
        guard let model = model as? TodayTaskCellModel else { return }
        
        self.taskDescriptionLabel.text = model.title
        self.taskDescriptionLabel.textColor = model.titleColor
        defer {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    //MARK:-SetupViews
    private func createTitleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.numberOfLines = 0
        label.preferredMaxLayoutWidth = bounds.width
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .red
        imageView.layer.masksToBounds = true
        contentView.addSubview(imageView)
        return imageView
    }
    //MARK:-SetupConstraint
    private func setupAllConstraintsToViews() {
        setupConstraintsToTitleImageView()
        setupConstraintsToTaskDescriptionLabel()
    }
    
    private func setupConstraintsToTitleImageView() {
        titleImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15.0).isActive = true
        titleImageView.centerYAnchor.constraint(equalTo: taskDescriptionLabel.centerYAnchor).isActive = true
        titleImageView.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
        titleImageView.widthAnchor.constraint(equalToConstant: 25.0).isActive = true
    }
    
    private func setupConstraintsToTaskDescriptionLabel() {
        taskDescriptionLabel.leftAnchor.constraint(equalTo: titleImageView.rightAnchor, constant: 15.0).isActive = true
        taskDescriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15.0).isActive = true
        taskDescriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true
        taskDescriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
}
