//
//  TestTableViewTableViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/24/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class TaskContainerTableViewCell: IdentifiableTableViewCell {

    lazy var tableView: AutoresizingTableView = self.createTableView()
    fileprivate var model: TodayTaskTableViewCellModel?
    
    //MARK:-Loading
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllConstraintsToViews()
        configureTableView()
        contentView.backgroundColor = .clear
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
        self.tableView.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let _ = viewModel as? TodayTasksViewModelInput else { return }
        guard let model = model as? TodayTaskTableViewCellModel else { return }
        
        self.model = model
        self.tableView.dataSource = model
        self.tableView.delegate = model
        
        defer {
            self.tableView.reloadData()
        }
    }
    //MARK:-SetupViews
    private func configureTableView() {
        self.tableView.backgroundColor = UIColor.white
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        self.tableView.reloadData()
        self.tableView.register(TaskTableViewCell.self, forCellReuseIdentifier: TaskTableViewCell.reuseIdentifier)
    }
    
    private func createTableView() -> AutoresizingTableView {
        let tableView = AutoresizingTableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isScrollEnabled = false
        tableView.layer.cornerRadius = 14.0
        tableView.layer.masksToBounds = true
        tableView.addShadow(UIColor.black.withAlphaComponent(0.1), radius: 14.0)
        tableView.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        contentView.addSubview(tableView)
        return tableView
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToTableView()
    }
    
    private func setupConstraintsToTableView() {
        tableView.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -15).isActive = true
        tableView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 15).isActive = true
        tableView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
    }
}
