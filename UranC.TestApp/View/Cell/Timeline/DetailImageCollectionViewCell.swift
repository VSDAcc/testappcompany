//
//  DetailImageCollectionViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/13/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class DetailImageCollectionViewCell: IdentifiableCollectionViewCell {
    
    lazy var imageView: ScaledHeightImageView = self.createScaledImageView()
    
    //MARK:-Loading
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupAllConstraintsToViews()
        self.contentView.layer.masksToBounds = true
        self.contentView.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.invalidateIntrinsicContentSize()
    }
    //MARK:-CellModelRepresentable
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let model = model as? DetailImageCellModel else { return }
        
        imageView.downloadImageUsingCache(stringURL: model.image)
        defer {
            self.imageView.invalidateIntrinsicContentSize()
        }
    }
    //MARK:-SetupViews
    private func createScaledImageView() -> ScaledHeightImageView {
        let imageView = ScaledHeightImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.invalidateIntrinsicContentSize()
        contentView.addSubview(imageView)
        return imageView
    }
    //MARK:-SetupConstraints
    fileprivate let offset: CGFloat = 15.0
    private func setupAllConstraintsToViews() {
        setupConstraintsToImageView()
    }
    
    private func setupConstraintsToImageView() {
        imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
    }
}
