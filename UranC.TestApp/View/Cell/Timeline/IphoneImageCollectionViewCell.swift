//
//  IphoneImageCollectionViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class IphoneImageCollectionViewCell: IdentifiableCollectionViewCell {
    
    lazy var iphoneTitleLabel: UILabel = self.createIphoneTitle()
    
    lazy var iphoneImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 14.0
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    //MARK:-Loading
    override func prepareForReuse() {
        super.prepareForReuse()
        self.iphoneImageView.image = nil
        self.iphoneTitleLabel.text = ""
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(iphoneImageView)
        self.setupAllConstraintsToViews()
        self.contentView.layer.cornerRadius = 14.0
        self.contentView.layer.masksToBounds = true
        self.contentView.backgroundColor = UIColor.white
        self.contentView.addShadow(UIColor.black.withAlphaComponent(0.1), radius: 14.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:-CellModelRepresentable
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let model = model as? IphoneImageCellModel else { return }
        
        self.iphoneImageView.downloadImageUsingCache(stringURL: model.image)
        self.iphoneTitleLabel.text = model.title
    }
    //MARK:-SetupViews
    private func createIphoneTitle() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22)
        label.adjustsFontSizeToFitWidth = true
        label.backgroundColor = .clear
        label.textAlignment = .center
        label.textColor = .white
        iphoneImageView.addSubview(label)
        return label
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToIphoneTitle()
        setupConstraintsToIphoneImageView()
    }
    
    private func setupConstraintsToIphoneTitle() {
        iphoneTitleLabel.topAnchor.constraint(equalTo: iphoneImageView.topAnchor).isActive = true
        iphoneTitleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        iphoneTitleLabel.widthAnchor.constraint(equalTo: iphoneImageView.widthAnchor).isActive = true
        iphoneTitleLabel.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
    }
    
    private func setupConstraintsToIphoneImageView() {
        iphoneImageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        iphoneImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        iphoneImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        iphoneImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
}
