//
//  IphoneListCollectionViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class IphoneListCollectionViewCell: IdentifiableCollectionViewCell {
    
    fileprivate let offset: CGFloat = 10.0
    fileprivate lazy var collectionView: UICollectionView = self.createCollectionView()
    
    //MARK:-Loading
    override func prepareForReuse() {
        super.prepareForReuse()
        self.collectionView.delegate = nil
        self.collectionView.dataSource = nil
        self.collectionView.setContentOffset(.zero, animated: false)
        self.collectionView.reloadData()
        self.collectionView.setNeedsLayout()
        self.collectionView.layoutIfNeeded()
        self.collectionView.invalidateIntrinsicContentSize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupAllConstraintsToViews()
        self.contentView.backgroundColor = UIColor.white
        self.contentView.layer.cornerRadius = 14.0
        self.contentView.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        flowLayout.invalidateLayout()
    }
    
    //MARK:-CellModelRepresentable
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let model = model as? IphoneListCellModel else { return }
        
        collectionView.delegate = model
        collectionView.dataSource = model
        collectionView.setContentOffset(model.contentOffset, animated: false)
        
        defer {
            self.collectionView.reloadData()
            self.collectionView.setNeedsLayout()
            self.collectionView.layoutIfNeeded()
            self.collectionView.invalidateIntrinsicContentSize()
        }
    }
    //MARK:-SetupViews
    private func createCollectionView() -> UICollectionView {
        let flowLayout = CenterdCollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionHeadersPinToVisibleBounds = true
        flowLayout.minimumInteritemSpacing = offset
        flowLayout.minimumLineSpacing = offset
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: offset, bottom: 0, right: offset)
        let collection = UICollectionView(frame: self.frame, collectionViewLayout: flowLayout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(IphoneImageCollectionViewCell.self, forCellWithReuseIdentifier: IphoneImageCollectionViewCell.reuseIdentifier)
        collection.backgroundColor = UIColor.white
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.alwaysBounceHorizontal = true
        collection.decelerationRate = .fast
        contentView.addSubview(collection)
        return collection
    }
    
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToCollectionView()
    }
    private func setupConstraintsToCollectionView() {
        collectionView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
    }
}
