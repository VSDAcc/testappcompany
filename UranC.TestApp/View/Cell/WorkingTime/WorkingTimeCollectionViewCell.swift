//
//  WorkingTimeCollectionViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/1/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class WorkingTimeCollectionViewCell: IdentifiableCollectionViewCell {
    
    fileprivate lazy var timeLabel: UILabel = self.createWorkingTimeLabel()
    fileprivate lazy var workingStatusButton: UIButton = self.createWorkingStatusButton()
    fileprivate lazy var workingDescriptionLabel: UILabel = self.createWorkingDescriptionLabel()
    fileprivate lazy var workingPlaceImage: UIImageView = self.createWorkingImageView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.timeLabel.text = ""
        self.workingDescriptionLabel.text = ""
        self.workingPlaceImage.image = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllConstraintsToViews()
        contentView.layer.cornerRadius = 14.0
        contentView.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:-CellModelRepresentable
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let model = model as? WorkingTimeCollectionViewCellModel else { return }
        
        self.workingPlaceImage.image = UIImage(named: model.image)
        self.workingDescriptionLabel.text = model.workingDescription
        self.workingStatusButton.setTitle(model.statusTitle, for: .normal)
        self.timeLabel.text = model.time
        
        defer {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    //MARK:-SetupViews
    private func createWorkingTimeLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22)
        label.adjustsFontSizeToFitWidth = true
        label.backgroundColor = .clear
        label.textAlignment = .left
        label.textColor = .white
        label.numberOfLines = 0
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createWorkingStatusButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Зараз", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .green
        button.layer.cornerRadius = 12.0
        button.clipsToBounds = true
        contentView.addSubview(button)
        return button
    }
    
    private func createWorkingDescriptionLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22)
        label.adjustsFontSizeToFitWidth = true
        label.backgroundColor = .clear
        label.textAlignment = .left
        label.textColor = .white
        label.numberOfLines = 0
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createWorkingImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 14.0
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
        contentView.sendSubviewToBack(imageView)
        return imageView
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToWorkingTimeLabel()
        setupConstraintsToWorkingStatusButton()
        setupConstraintsToWorkingDescriptionLabel()
        setupConstraintsToWorkingPlaceImageView()
    }
    
    private func setupConstraintsToWorkingTimeLabel() {
        timeLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        timeLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        timeLabel.rightAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }

    private func setupConstraintsToWorkingDescriptionLabel() {
        workingDescriptionLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        workingDescriptionLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 10).isActive = true
        workingDescriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        workingDescriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    private func setupConstraintsToWorkingStatusButton() {
        workingStatusButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true
        workingStatusButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        workingStatusButton.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        workingStatusButton.heightAnchor.constraint(equalToConstant: 25.0).isActive = true
    }
    
    private func setupConstraintsToWorkingPlaceImageView() {
        workingPlaceImage.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        workingPlaceImage.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        workingPlaceImage.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        workingPlaceImage.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
    }
}
