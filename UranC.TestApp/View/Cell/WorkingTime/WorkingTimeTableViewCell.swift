//
//  WorkingTimeTableViewCell.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class WorkingTimeTableViewCell: IdentifiableTableViewCell {

    fileprivate lazy var titleLabel: UILabel = self.createTitleLabel()
    fileprivate lazy var moreTimeButton: UIButton = self.createAddMoreWorkingTimeButton()
    fileprivate lazy var collectionView: UICollectionView = self.createCollectionView()
    fileprivate var itemsOffset: CGFloat = 15.0
    
    //MARK:-Loading
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllConstraintsToViews()
        configureCollectionView()
        contentView.backgroundColor = .white
        contentView.addShadow(UIColor.black.withAlphaComponent(0.1), radius: 14.0)
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.collectionView.delegate = nil
        self.collectionView.dataSource = nil
        self.collectionView.reloadData()
        self.collectionView.setNeedsLayout()
        self.collectionView.layoutIfNeeded()
        self.collectionView.invalidateIntrinsicContentSize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        flowLayout.invalidateLayout()
    }
    
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let _ = viewModel as? TodayTasksViewModelInput else { return }
        guard let model = model as? WorkingTimeCellModel else { return }
        
        self.collectionView.dataSource = model
        self.collectionView.delegate = model
        self.titleLabel.text = model.title
        
        defer {
            self.collectionView.reloadData()
            self.collectionView.setNeedsLayout()
            self.collectionView.layoutIfNeeded()
            self.collectionView.invalidateIntrinsicContentSize()
        }
    }
    
    private func configureCollectionView() {
        collectionView.register(WorkingTimeCollectionViewCell.self, forCellWithReuseIdentifier: WorkingTimeCollectionViewCell.reuseIdentifier)
    }
    //MARK:-SetupViews
    private func createTitleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 22, weight: .regular)
        label.numberOfLines = 0
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createAddMoreWorkingTimeButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = 12.0
        button.clipsToBounds = true
        contentView.addSubview(button)
        return button
    }
    
    private func createCollectionView() -> UICollectionView {
        let flowLayout = CenterdCollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionHeadersPinToVisibleBounds = true
        flowLayout.headerReferenceSize = .zero
        flowLayout.minimumInteritemSpacing = itemsOffset
        flowLayout.minimumLineSpacing = itemsOffset
        flowLayout.sectionInset = UIEdgeInsets(top: itemsOffset, left: itemsOffset, bottom: itemsOffset, right: itemsOffset)
        let collection = UICollectionView(frame: self.frame, collectionViewLayout: flowLayout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = UIColor.white
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.alwaysBounceHorizontal = true
        collection.decelerationRate = .fast
        contentView.addSubview(collection)
        return collection
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToMoreTimeButton()
        setupConstraintsToTitleLabel()
        setupConstraintsToCollectionView()
    }
    
    private func setupConstraintsToMoreTimeButton() {
        moreTimeButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -itemsOffset).isActive = true
        moreTimeButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: itemsOffset).isActive = true
        moreTimeButton.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        moreTimeButton.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
    }
    
    private func setupConstraintsToTitleLabel() {
        titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: itemsOffset).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: itemsOffset).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: moreTimeButton.leftAnchor, constant: -itemsOffset).isActive = true
    }
    
    private func setupConstraintsToCollectionView() {
        collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5).isActive = true
        collectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
    }
}
