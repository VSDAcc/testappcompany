//
//  RegistrationInfoHeaderView.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/15/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class RegistrationInfoHeaderView: UIView {

    lazy var titleLabel: UILabel = self.createTitleLabel()
    //MARK:-Loading
    init() {
        super.init(frame: UIScreen.main.bounds)
        setupAllConstraintsToViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK:-SetupViews
    private func createTitleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        addSubview(label)
        return label
    }
    //MARK:-SetupConstraints
    fileprivate let itemsOffset: CGFloat = 15.0
    private func setupAllConstraintsToViews() {
        setupConstraintsToHeaderTitleLabel()
    }
    
    private func setupConstraintsToHeaderTitleLabel() {
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
}
