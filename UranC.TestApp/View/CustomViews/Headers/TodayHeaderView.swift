//
//  TodayHeaderView.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/28/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class TodayHeaderView: UIView {
    
    lazy var titleLabel: UILabel = self.createTitleLabel()
    lazy var dateLabel: UILabel = self.createDateLabel()
    
    //MARK:-Loading
    init() {
        super.init(frame: UIScreen.main.bounds)
        setupAllConstraintsToViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    //MARK:-SetupViews
    private func createTitleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.sizeToFit()
        addSubview(label)
        return label
    }
    
    private func createDateLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.sizeToFit()
        addSubview(label)
        return label
    }
    //MARK:-SetupConstraints
    fileprivate let itemsOffset: CGFloat = 10.0
    fileprivate var titleLabelWidthConstraint: NSLayoutConstraint?
    private func setupAllConstraintsToViews() {
        setupConstraintsToHeaderTitleLabel()
        setupConstraintsToHeaderDateLabel()
    }
    
    private func setupConstraintsToHeaderDateLabel() {
        dateLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        dateLabel.topAnchor.constraint(equalTo: topAnchor, constant: itemsOffset).isActive = true
    }
    
    private func setupConstraintsToHeaderTitleLabel() {
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: itemsOffset).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
    }
}
