//
//  TimelineHeaderView.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/2/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class TimelineHeaderView: UICollectionReusableView {
    
    lazy var headerImageView: UIImageView = self.createImageView()
    //MARK:-Loading
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupAllConstraintsToViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    //MARK:-SetupViews
    private func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named:"Venice")
        imageView.clipsToBounds = true
        addSubview(imageView)
        return imageView
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToHeaderImageView()
    }
    
    private func setupConstraintsToHeaderImageView() {
        headerImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        headerImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        headerImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        headerImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
