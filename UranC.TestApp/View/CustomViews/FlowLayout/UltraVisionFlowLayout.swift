//
//  UltraVisionFlowLayout.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/4/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit

// The heights are declared as constants outside of the class so they can be easily referenced elsewhere
struct UltravisualLayoutConstants {
    struct Cell {
        // The height of the non-featured cell
        static let standardSize: CGSize = CGSize(width: 250, height: 150)
        // The height of the first visible cell
        static let featuredSize: CGSize = CGSize(width: 300, height: 186)
    }
}

// MARK: Properties and Variables

class UltravisualLayout: UICollectionViewFlowLayout {
    // The amount the user needs to scroll before the featured cell changes
    var dragOffset: CGFloat {
        return collectionView?.bounds.width ?? 300.0
    }
    
    var cache: [UICollectionViewLayoutAttributes] = []
    
    // Returns the item index of the currently featured cell
    var featuredItemIndex: Int {
        // Use max to make sure the featureItemIndex is never < 0
        return max(0, Int(collectionView!.contentOffset.x / dragOffset))
    }
    
    // Returns a value between 0 and 1 that represents how close the next cell is to becoming the featured cell
    var nextItemPercentageOffset: CGFloat {
        return (collectionView!.contentOffset.x / dragOffset) - CGFloat(featuredItemIndex)
    }
    
    // Returns the width of the collection view
    var width: CGFloat {
        return collectionView!.bounds.width
    }
    
    // Returns the height of the collection view
    var height: CGFloat {
        return collectionView!.bounds.height
    }
    
    // Returns the number of items in the collection view
    var numberOfItems: Int {
        return collectionView!.numberOfItems(inSection: 0)
    }
}

// MARK: UICollectionViewLayout

extension UltravisualLayout {
    // Return the size of all the content in the collection view
    override var collectionViewContentSize : CGSize {
        let contentWidth = (CGFloat(numberOfItems) * dragOffset) + (width - dragOffset)
        return CGSize(width: contentWidth, height: height)
    }
    
    override func prepare() {
        cache.removeAll(keepingCapacity: false)
        
        let standartSize = UltravisualLayoutConstants.Cell.standardSize
        let featuredSize = UltravisualLayoutConstants.Cell.featuredSize
        
        var frame = CGRect.zero
        var x: CGFloat = 0.0
        
        for item in 0..<numberOfItems {
            let indexPath = IndexPath(item: item, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            attributes.zIndex = item
            var size = standartSize
            
            if indexPath.item == featuredItemIndex {
                let xOffset = standartSize.width * nextItemPercentageOffset - 30.0
                x = collectionView!.contentOffset.x - xOffset
                size = featuredSize
            } else if indexPath.item == featuredItemIndex + 1 && indexPath.item != numberOfItems {
                let maxX = x + standartSize.width
                
                let height = standartSize.height + max((featuredSize.height - standartSize.height) * nextItemPercentageOffset, 0)
                let width = standartSize.width + max((featuredSize.width - standartSize.width) * nextItemPercentageOffset, 0)
                size = CGSize(width: width, height: height)
                
                x = maxX - width + 30.0
            }
            frame = CGRect(x: x, y: 0, width: size.width, height: size.height)
            attributes.frame = frame
            x = frame.maxX
            cache.append(attributes)
        }
    }
    
    // Return all attributes in the cache whose frame intersects with the rect passed to the method
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes: [UICollectionViewLayoutAttributes] = []
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
    // Return the content offset of the nearest cell which achieves the nice snapping effect, similar to a paged UIScrollView
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let itemIndex = round(proposedContentOffset.x / dragOffset)
        let xOffset = itemIndex * dragOffset
        return CGPoint(x: xOffset, y: 0)
    }
    
    // Return true so that the layout is continuously invalidated as the user scrolls
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
