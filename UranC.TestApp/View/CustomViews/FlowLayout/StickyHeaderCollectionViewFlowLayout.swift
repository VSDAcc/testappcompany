//
//  StickyHeaderCollectionViewFlowLayout.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/3/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

class StickyHeaderCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)
        
        let offset = collectionView!.contentOffset
        if offset.y < 0 {
            let deltaY = abs(offset.y)
            layoutAttributes?.forEach({ (attribute) in
                if let elementOfKind = attribute.representedElementKind {
                    if elementOfKind == UICollectionView.elementKindSectionHeader {
                        var frame = attribute.frame
                        frame.size.height = max(0, headerReferenceSize.height + deltaY)
                        frame.origin.y = CGRect().minY - deltaY
                        attribute.frame = frame
                    }
                }
            })
        }
        
        return layoutAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
