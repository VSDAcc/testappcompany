//
//  TableViewInTableViewCellViewController.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/24/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol TodayViewControllerInput: TodayTasksViewModelOutput { }

class TodayViewController: UIViewController, TodayViewControllerInput {
    
    fileprivate lazy var tableView: UITableView = self.createTableView()
    fileprivate lazy var headerView: TodayHeaderView = self.createTodayHeaderView()
    
    fileprivate var estimatedCellHeights: [IndexPath : CGFloat] = [:]
    fileprivate var defaultHeightForCell: CGFloat = 280.0
    fileprivate let animator = UIViewPropertyAnimator(duration: 0.3, curve: .linear)
    
    fileprivate let viewModel: TodayTasksViewModelInput
    
    //MARK:-Loading
    init(viewModel: TodayTasksViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAllConstraintsToViews()
        configureTableView()
        view.backgroundColor = .white
        viewModel.loadData()
        viewModel.loadHeaderData()
        navigationItem.rightBarButtonItem = createMapBarButtonItem()
        animator.addAnimations { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.headerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3).translatedBy(x: 0, y: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.tableView.tableHeaderView?.layoutIfNeeded()
            self.tableView.tableHeaderView = self.tableView.tableHeaderView
        }
    }
    //MARK:-TodayViewControllerInput
    func viewModelDidLoadData(_ viewModel: TodayTasksViewModelInput) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func viewModel(_ viewModel: TodayTasksViewModelInput, didLoadHeader header: TodayHeaderModel) {
        DispatchQueue.main.async {
            self.headerView.titleLabel.text = header.title
            self.headerView.dateLabel.text = header.date
        }
    }
    
    func viewModelScrollToBottom(_ viewModel: TodayTasksViewModelInput) {
        DispatchQueue.main.async {
            self.tableView.scrollRectToVisible(CGRect(x: 0, y: self.tableView.contentSize.height - self.tableView.bounds.size.height, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height), animated: true)
            self.tableView.setNeedsLayout()
            self.tableView.layoutIfNeeded()
        }
    }
    
    func viewModel(_ viewModel: TodayTasksViewModelInput, needsReload section: IndexSet) {
        DispatchQueue.main.async {
            self.tableView.reloadSections(section, with: .automatic)
            self.tableView.setNeedsLayout()
            self.tableView.layoutIfNeeded()
            self.tableView.invalidateIntrinsicContentSize()
        }
    }
    //MARK:-Actions
    @objc func actionMapButtonDidPressed(_ sender: UIBarButtonItem) {
        viewModel.showTodayMapViewController()
    }
    //MARK:-SetupViews
    private func configureTableView() {
        self.tableView.backgroundColor = .white
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = headerView
        self.tableView.tableHeaderView?.layoutIfNeeded()
        self.tableView.estimatedRowHeight = defaultHeightForCell
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(TaskContainerTableViewCell.self, forCellReuseIdentifier: TaskContainerTableViewCell.reuseIdentifier)
        self.tableView.register(WorkingTimeTableViewCell.self, forCellReuseIdentifier: WorkingTimeTableViewCell.reuseIdentifier)
    }
    
    private func createTableView() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        return tableView
    }
    
    private func createTodayHeaderView() -> TodayHeaderView {
        let headerView = TodayHeaderView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = .clear
        tableView.addSubview(headerView)
        return headerView
    }
    
    private func createMapBarButtonItem() -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(title: "Map", style: .done, target: self, action: #selector(actionMapButtonDidPressed(_ :)))
        barButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .regular),
            NSAttributedString.Key.foregroundColor: UIColor.blue
            ], for: .normal)
        return barButtonItem
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToTableView()
        setupConstraintsHeaderView()
    }
    
    private func setupConstraintsToTableView() {
        tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
    }
    
    private func setupConstraintsHeaderView() {
        headerView.topAnchor.constraint(equalTo: tableView.topAnchor).isActive = true
        headerView.leftAnchor.constraint(equalTo: tableView.leftAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor).isActive = true
    }
}
extension TodayViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let registrationModel = viewModel.selectedItemAt(section: indexPath.section, row: indexPath.row)
        let registrationCell = tableView.dequeueReusableCell(withIdentifier: registrationModel.cellIdentifier, for: indexPath) as! IdentifiableTableViewCell
        registrationCell.updateModel(registrationModel, viewModel: viewModel)
        return registrationCell
    }
}
extension TodayViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let taskContainer = cell as? TaskContainerTableViewCell {
            let itemOffset: CGFloat = 35.0
            estimatedCellHeights[indexPath] = taskContainer.tableView.contentSize.height + itemOffset
        } else {
            estimatedCellHeights[indexPath] = defaultHeightForCell
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return estimatedCellHeights[indexPath] ?? defaultHeightForCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return estimatedCellHeights[indexPath] ?? defaultHeightForCell
    }
}
extension TodayViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            animator.fractionComplete = abs(scrollView.contentOffset.y) / 150
        }
    }
}
