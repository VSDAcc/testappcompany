//
//  TodayMapsViewController.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 1/5/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit
import GoogleMaps

protocol TodayMapsViewControllerInput: TodayMapsViewModelOutput { }

class TodayMapsViewController: UIViewController, TodayMapsViewControllerInput {

    fileprivate lazy var mapView: GMSMapView = self.createMapView()
    fileprivate lazy var addressLabel: UILabel = self.createMapCoordinateLabel()
    fileprivate var locationManager: CLLocationManager = CLLocationManager()
    
    fileprivate let viewModel: TodayMapsViewModelInput
    
    //MARK:-Loading
    init(viewModel: TodayMapsViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAllConstraintsToViews()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        viewModel.loadGoogleMapsData()
    }
    
    fileprivate func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { [weak self] (response, error) in
            guard let strongSelf = self,
                let address = response?.firstResult(),
                let lines = address.lines else {
                    return
            }
            strongSelf.addressLabel.unlock()
            strongSelf.addressLabel.text = lines.joined(separator: "\n")
            strongSelf.mapView.padding = UIEdgeInsets(top: strongSelf.view.safeAreaInsets.top, left: 0, bottom: (strongSelf.view.bounds.height / 9), right: 0)
            
            UIView.animate(withDuration: 0.3, animations: {
                strongSelf.view.layoutIfNeeded()
            })
        }
    }
    //MARK:-TodayMapsViewControllerInput
    func viewModelDidLoadData(_ viewModel: TodayMapsViewModelInput) {
        
    }
    
    func viewModelWillLoadData(_ viewModel: TodayMapsViewModelInput) {
    
    }
    
    func viewModel(_ viewModel: TodayMapsViewModelInput, didLoadMapMarker marker: PlaceMarker) {
        self.mapView.clear()
        marker.map = self.mapView
    }
    //MARK:-SetupViews
    private func createMapView() -> GMSMapView {
        let mapView = GMSMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.delegate = self
        view.addSubview(mapView)
        return mapView
    }
    
    private func createMapCoordinateLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18)
        label.adjustsFontSizeToFitWidth = true
        label.backgroundColor = .white
        label.alpha = 0.85
        label.textAlignment = .center
        label.textColor = .black
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        view.addSubview(label)
        view.bringSubviewToFront(label)
        return label
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToMapView()
        setupConstraintsToAdressLabel()
    }
    
    private func setupConstraintsToMapView() {
        mapView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        mapView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
    }
    
    private func setupConstraintsToAdressLabel() {
        addressLabel.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        addressLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        addressLabel.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
    }
}
extension TodayMapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else { return }
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        DispatchQueue.main.async {
            self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.locationManager.stopUpdatingLocation()
        }
        viewModel.updateGoogleMapsData(location.coordinate)
    }
}
extension TodayMapsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        DispatchQueue.main.async {
            self.reverseGeocodeCoordinate(position.target)
        }
        viewModel.updateGoogleMapsData(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        DispatchQueue.main.async {
            self.addressLabel.lock()
            if (gesture) {
                mapView.selectedMarker = nil
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else { return nil }
        let label = UILabel()
        label.text = placeMarker.place.name
        label.textColor = .black
        label.sizeToFit()
        return label
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        DispatchQueue.main.async {
            mapView.selectedMarker = nil
        }
        return false
    }
}
