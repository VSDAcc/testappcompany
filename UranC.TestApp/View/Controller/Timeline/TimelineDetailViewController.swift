//
//  TimelineDetailViewController.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 12/13/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol TimelineDetailViewControllerInput: DetailImageViewModelOutput { }

class TimelineDetailViewController: UIViewController, TimelineDetailViewControllerInput {

    fileprivate lazy var collectionView: UICollectionView = self.createCollectionView()
    fileprivate lazy var navigationBar: UINavigationBar = self.createNavigationBar()
    fileprivate let itemOffset: CGFloat = 10.0
    fileprivate var isInitialScrollDone: Bool = false
    
    weak var selectedCell: DetailImageCollectionViewCell?
    var animatableIphoneCells: [UICollectionViewCell]?
    
    fileprivate let viewModel: DetailImageViewModelInput
    
    //MARK:-Loading
    init(viewModel: DetailImageViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.view = self
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadData()
        setupAllConstraintsToView()
        view.backgroundColor = .black
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.viewModel.iphoneListNeedsToScroll()
        view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !isInitialScrollDone {
            isInitialScrollDone = true
            scrollCollectionViewToIndexPath(viewModel.selectedIndex)
            selectedCell = collectionView.cellForItem(at: viewModel.selectedIndex) as? DetailImageCollectionViewCell
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.scrollCollectionViewToIndexPath(self.viewModel.selectedIndex)
            self.findSelectedCell()
        }
        flowLayout.invalidateLayout()
    }
    //MARK:-Actions
    @objc func actionCancelButtonDidPressed(_ sender: UIBarButtonItem) {
        viewModel.dismissDetailImageViewController(true)
    }
    
    private func scrollCollectionViewToIndexPath(_ indexPath: IndexPath) {
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        self.collectionView.setNeedsLayout()
        self.collectionView.layoutIfNeeded()
    }
    
    fileprivate func findSelectedCell() {
        var currentIndexPath: IndexPath = self.viewModel.selectedIndex
        let center = self.view.convert(self.collectionView.center, to: self.collectionView)
        currentIndexPath = self.collectionView.indexPathForItem(at: center) ?? currentIndexPath
        self.viewModel.selectedIndex = currentIndexPath
        self.selectedCell = self.collectionView.cellForItem(at: self.viewModel.selectedIndex) as? DetailImageCollectionViewCell
    }
    //MARK:-TimelineDetailViewControllerInput
    func viewModelDidLoadData(_ viewModel: DetailImageViewModelInput) {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func viewModelWillLoadData(_ viewModel: DetailImageViewModelInput) {
        
    }
    
    func viewModel(_ viewModel: DetailImageViewModelInput, didHandleError error: String) {
        
    }
    
    func viewModel(_ viewModel: DetailImageViewModelInput, didSelectItemAt indexPath: IndexPath) {
        scrollCollectionViewToIndexPath(indexPath)
    }
    //MARK:-SetupViews
    private func createNavigationBar() -> UINavigationBar {
        let navigationBar = UINavigationBar()
        let navigationItem = UINavigationItem()
        navigationItem.leftBarButtonItem = createCancelButtonItem()
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.setItems([navigationItem], animated: false)
        navigationBar.isTranslucent = true
        navigationBar.shadowImage = UIImage()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        view.addSubview(navigationBar)
        return navigationBar
    }
    
    private func createCancelButtonItem() -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(actionCancelButtonDidPressed(_ :)))
        barButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .regular),
            NSAttributedString.Key.foregroundColor: UIColor.white
            ], for: .normal)
        return barButtonItem
    }
    //MARK:-SetupViews
    private func createCollectionView() -> UICollectionView {
        let flowLayout = CenterdCollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionHeadersPinToVisibleBounds = true
        flowLayout.headerReferenceSize = .zero
        flowLayout.minimumInteritemSpacing = itemOffset
        flowLayout.minimumLineSpacing = itemOffset
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let collection = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(DetailImageCollectionViewCell.self, forCellWithReuseIdentifier: DetailImageCollectionViewCell.reuseIdentifier)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.clear
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.alwaysBounceHorizontal = true
        collection.decelerationRate = .fast
        view.addSubview(collection)
        return collection
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToView() {
        setupConstraintsToNavigationBar()
        setupConstraintsToCollectionView()
    }
    
    private func setupConstraintsToNavigationBar() {
        navigationBar.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        navigationBar.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        navigationBar.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        navigationBar.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
    }
    
    private func setupConstraintsToCollectionView() {
        collectionView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    }
}
extension TimelineDetailViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let iphoneCellModel = viewModel.selectedItemAt(section: indexPath.section, row: indexPath.row)
        let iphoneCell = collectionView.dequeueReusableCell(withReuseIdentifier: iphoneCellModel.cellIdentifier, for: indexPath) as! IdentifiableCollectionViewCell
        iphoneCell.updateModel(iphoneCellModel, viewModel: viewModel)
        return iphoneCell
    }
}
extension TimelineDetailViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
    }
}
extension TimelineDetailViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIApplication.shared.statusBarOrientation.isLandscape {
            let width: CGFloat = collectionView.bounds.width
            let height: CGFloat = collectionView.bounds.height
            let size = CGSize(width: width, height: height)
            return size
        } else {
            let width: CGFloat = collectionView.bounds.width
            let height: CGFloat = collectionView.bounds.height
            let size = CGSize(width: width, height: height)
            return size
        }
    }
}
extension TimelineDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        findSelectedCell()
    }
}
extension TimelineDetailViewController: ListToDetailAnimatable {

    var morphViews: [UIView] {
        return [selectedCell!.imageView]
    }
    
    var animatableCells: [UICollectionViewCell] {
        return animatableIphoneCells!
    }
}
