//
//  TimelineViewController.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 19.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import UIKit

protocol TimelineViewControllerInput: TimelineViewModelOutput { }

class TimelineViewController: UIViewController, TimelineViewControllerInput {
    
    fileprivate lazy var collectionView: UICollectionView = self.createCollectionView()
    fileprivate lazy var registrationButton: UIBarButtonItem = self.createNewsBarButtonItem()
    fileprivate let offset: CGFloat = 10.0
    fileprivate lazy var refreshControl: UIRefreshControl = self.createRefreshContol()
    fileprivate var lastContentOffset: CGFloat = 0
    
    weak var selectedCell: IphoneImageCollectionViewCell?
    var animatableIphoneCells: [UICollectionViewCell] {
        return collectionView.visibleCells.filter({ $0 != selectedCell })
    }
    
    fileprivate let viewModel: TimelineViewModelInput
    
    //MARK:-Loading
    init(viewModel: TimelineViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAllConstraintsToViews()
        viewModel.loadData()
        viewModel.updateExhibitList()
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = self.registrationButton
        collectionView.addSubview(refreshControl)
        collectionView.bringSubviewToFront(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        flowLayout.invalidateLayout()
    }
    //MARK:-Actions
    @objc func actionTodayButtonDidPressed(_ sender: UIBarButtonItem) {
        viewModel.showTodayViewController()
    }
    
    @objc func pullToRefresh(_ sender: UIRefreshControl) {
        viewModel.updateExhibitList()
    }
    //MARK:-TimelineViewControllerInput
    func viewModelDidLoadData(_ viewModel: TimelineViewModelInput) {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.setNeedsLayout()
            self.collectionView.layoutIfNeeded()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func viewModelWillLoadData(_ viewModel: TimelineViewModelInput) {
        
    }
    
    func viewModel(_ viewModel: TimelineViewModelInput, didHandleError error: String) {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func viewModel(_ viewModel: TimelineViewModelInput, didSelectCell cell: UICollectionViewCell) {
        if let selectedCell = cell as? IphoneImageCollectionViewCell {
            self.selectedCell = selectedCell
        }
    }
    //MARK:-SetupViews
    private func createCollectionView() -> UICollectionView {
        let flowLayout = StickyHeaderCollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionHeadersPinToVisibleBounds = false
        flowLayout.headerReferenceSize = CGSize(width: view.bounds.width, height: headerViewHeightConstant)
        flowLayout.minimumInteritemSpacing = offset
        flowLayout.minimumLineSpacing = offset
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let collection = UICollectionView(frame: view.frame, collectionViewLayout: flowLayout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.register(IphoneListCollectionViewCell.self, forCellWithReuseIdentifier: IphoneListCollectionViewCell.reuseIdentifier)
        collection.register(TimelineHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TimelineHeaderView.reuseIdentifier)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.clear
        collection.showsVerticalScrollIndicator = false
        collection.showsHorizontalScrollIndicator = false
        collection.alwaysBounceVertical = true
        view.addSubview(collection)
        return collection
    }
    
    private func createNewsBarButtonItem() -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(title: "Today", style: .done, target: self, action: #selector(actionTodayButtonDidPressed(_ :)))
        barButtonItem.setTitleTextAttributes([
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .regular),
            NSAttributedString.Key.foregroundColor: UIColor.blue
            ], for: .normal)
        return barButtonItem
    }
    
    private func createRefreshContol() -> UIRefreshControl {
        let refresh = UIRefreshControl()
        refresh.backgroundColor = .clear
        refresh.tintColor = .white
        refresh.layer.zPosition = 99
        refresh.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
        return refresh
    }
    //MARK:-SetupConstraints
    fileprivate let headerViewHeightConstant: CGFloat = 150.0
    
    private func setupAllConstraintsToViews() {
        setupConstraintsToCollectionView()
    }
    
    private func setupConstraintsToCollectionView() {
        collectionView.topAnchor.constraint(lessThanOrEqualTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    }
}
extension TimelineViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let iphoneCellModel = viewModel.selectedItemAt(section: indexPath.section, row: indexPath.row)
        let iphoneCell = collectionView.dequeueReusableCell(withReuseIdentifier: iphoneCellModel.cellIdentifier, for: indexPath) as! IdentifiableCollectionViewCell
        iphoneCell.updateModel(iphoneCellModel, viewModel: viewModel)
        return iphoneCell
    }
}
extension TimelineViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
    }
}
extension TimelineViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: TimelineHeaderView.reuseIdentifier, for: indexPath)
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: headerViewHeightConstant)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIApplication.shared.statusBarOrientation.isLandscape {
            let width: CGFloat = collectionView.bounds.width / 2 - offset
            let height: CGFloat = collectionView.bounds.height / 1.5 - offset * 2
            let size = CGSize(width: width, height: height)
            return size
        } else {
            let width: CGFloat = collectionView.bounds.width - offset
            let height: CGFloat = (collectionView.bounds.height / 2.5) - offset
            let size = CGSize(width: width, height: height)
            return size
        }
    }
}
extension TimelineViewController: ListToDetailAnimatable {

    var morphViews: [UIView] {
        return [selectedCell!.iphoneImageView]
    }

    var animatableCells: [UICollectionViewCell] {
        return animatableIphoneCells
    }
}
