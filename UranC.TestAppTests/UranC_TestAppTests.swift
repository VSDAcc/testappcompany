//
//  UranC_TestAppTests.swift
//  UranC.TestAppTests
//
//  Created by Vladymyr on 24.06.2018.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import XCTest
@testable import UranC_TestApp

class UranC_TestAppTests: XCTestCase {
    
    var sut: TimelineViewModelInput!
    var apiService: MockFileExhibitsLoader!
    
    override func setUp() {
        super.setUp()
        apiService = MockFileExhibitsLoader()
        sut = TimelineViewModel(apiService)
    }
    
    override func tearDown() {
        sut = nil
        apiService = nil
        super.tearDown()
    }
    
    func testFetchIphonesError() {
        //Given
        let numberOfItem = 1
        //When
        sut.getExhibitList()
        apiService.fetchSuccess()
        //Then
        XCTAssertNotNil(sut)
        XCTAssertNotNil(apiService)
        XCTAssertEqual(sut.numberOfItemsInSection(section: 0), numberOfItem)
    }
    
}













